"""
Contains the model of a schedule for the schedule optimization. Furthermore
this modul got some convenience methods to deal with schedules.

The core class is "Schedule()", which creates an empty schedule. You can build a
Schedule with the Schedule.Builder. To obtain a Builder you just have to invoke Schedule.builder().
"""
from enum import Enum
import copy

class Strategy(Enum):
    """
    Strategy-Constants for a single Slot.
    """

    # storage can decide
    BUY = (0, True, True)
    SELL = (1, False, True)
    DISCHARGE = (2, False, True)
    CHARGE = (3, True, True)

    # only for on-site power requiremens
    OWN_DISCHARGE = (4, False, True)

    # storage can not decide
    BLOCKED_CHARGE = (5, True, False)
    BLOCKED_DISCHARGE = (6, False, False)

    def __init__(self, id, is_charge, selectable):
        self.__id = id
        self.__is_charge = is_charge
        self.__selectable = selectable

    @property
    def is_charge(self):
        return self.__is_charge

    @property
    def selectable(self):
        return self.__selectable

    @property
    def id(self):
        return self.__id

    @staticmethod
    def selectables():
        return [strategy for strategy in list(Strategy) if strategy.selectable]

    @staticmethod
    def filter_by_type(is_charge):
        return [strategy for strategy in list(Strategy) if strategy.is_charge == is_charge]

    @staticmethod
    def filter(is_charge, selectable):
        return [strategy for strategy in list(Strategy) if strategy.is_charge == is_charge and strategy.selectable == selectable]

    @staticmethod
    def from_id(identifier):
        return [strategy for strategy in list(Strategy) if strategy.id == identifier][0]

class Slot:
    """
    Mutable model for a time-slot in a schedule. Holds the information when the slot starts,
    ends and how the current load of the storage will be.
    """

    def __init__(self, load_state, start, end, strategy=Strategy.DISCHARGE):
        self.__start = start
        self.__end = end
        self.__load_state = load_state
        self.__strategy = strategy

    def __eq__(self, other):
        return isinstance(other, self.__class__) \
                    and self.__start == other.start \
                    and self.__end == other.end \
                    and self.__load_state == other.load_state \
                    and self.__strategy == other.strategy

    def __str__(self):
        return "[ %s, %s, %s, %s ]" % (self.__load_state, self.__strategy, self.__end, self.__start)

    def __repr__(self):
        return self.__str__()

    def copy(self):
        return Slot(self.load_state, self.start, self.end, self.strategy)

    @property
    def start(self):
        """
        Property for starting time
        """
        return self.__start

    @property
    def end(self):
        """
        Property for ending time
        """
        return self.__end

    @property
    def load_state(self):
        """
        Property for the load_state
        """
        return self.__load_state

    @load_state.setter
    def load_state(self, load_state):
        """
        Sets the load state in percent in the slot
        """
        self.__load_state = load_state

    @property
    def strategy(self):
        """
        Property for the strategy
        """
        return self.__strategy

    @strategy.setter
    def strategy(self, strategy):
        """
        Set the strategy
        """
        self.__strategy = strategy

    def inside(self, start, end, overlapping=False):
        """
        Determines whether the Slot is inside the given time interval.

        :param start: start of the interval
        :param end: end of the interval
        :overlapping: default=False, if true inside will also return True when the Slot overlapps
        :returns: True if inside the interval
        """
        return self.__start >= start and self.__end <= end \
                or (
                    overlapping
                        and (
                            (self.__start >= start and self.__start <= end)
                            or (self.__end >= start and self.__end <= end)
                            or (self.__start <= start and self.__end >= end)
                        )
                )

class Schedule:
    """
    Mutable model for a schedule.
    """

    class Builder:
        """
        Builder of a Schedule
        """

        def __init__(self):
            self.__slots = []

        def add_slot_duration(self, load_state, duration, strategy=Strategy.DISCHARGE):
            """
            Adds a slot to the builder.

            :param load_state: load_state after the interval
            :param duration: duration of the interval
            :returns: self to chain methods
            """
            last_end = 0 if len(self.__slots) == 0 else self.__slots[-1].end
            self.add_slot(load_state, last_end, last_end + duration, strategy)
            return self

        def add_slot(self, load_state, start, end, strategy=Strategy.DISCHARGE):
            """
            Adds a slot to the builder.

            :param load_state: load_state after the interval
            :param start: start time
            :param end: end time
            :returns: self to chain methods
            """
            self.__slots.append(Slot(load_state, start, end, strategy))
            return self

        def add_none(self, duration):
            self.__slots.append(None)
            return self

        def build(self):
            """
            Build the schedule with the provided slots.
            """
            return Schedule(self.__slots)

    def __init__(self, slots):
        self.__slots = slots

    def __str__(self):
        return "%s" % (self.__slots)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__slots == other.slots

    def get_slot(self, index):
        """
        Provides list of slots.

        :param index: index of the slot
        :returns: the Slot with the index <code>index</code>
        """
        return self.__slots[index]

    def get_slots_in_interval(self, start, end, overlapping=False):
        """
        provides list of slots in a specific interval.

        :param start: starting time
        :param end: ending time
        :returns: all Slots in the given interval
        """
        return [slot for slot in self.__slots if slot.inside(start, end, overlapping)]

    @property
    def slots(self):
        """
        :returns: list of slots
        """
        return self.__slots

    def slots_copy(self):
        slots = []
        for slot in self.__slots:
            slots.append(slot.copy())
        return slots

    def size(self):
        """
        :returns: number of slots
        """
        return len(self.__slots)

    def __deepcopy__(self, add):
        return self.copy()

    def copy(self):
        slots = []
        for slot in self.__slots:
            slots.append(slot.copy())
        return Schedule(slots)

    @staticmethod
    def builder():
        """
        Creates a Builder for the Schedule.

        :returns: new Builder
        """
        return Schedule.Builder()
