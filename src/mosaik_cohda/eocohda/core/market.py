"""
Contains different EnergyMarket models and implementations.
"""
from interface import implements, Interface
from overrides import overrides
import numpy as np


class EnergyMarket(Interface):
    """
    Interface for an energy market.
    """

    def offer(self, energy, interval_num):
        """
        Offer an amount of energy to the market

        :param energy: energy to offer
        :param interval_num: number of interval
        :returns: money you would get for that energy in the interval
        """
        pass

    def ask(self, energy, interval_num):
        """
        Ask for an offer for a specific amount of energy in an specific interval

        :param energy: energy to offer
        :param interval_num: number of interval
        :returns: money you would have to pay for that slot
        """
        pass

class TestMarket(implements(EnergyMarket)):
    """
    Market-Implementation for testing
    """

    @overrides
    def offer(self, energy, interval_num):
        """
        Override
        """
        return 1 * energy

    @overrides
    def ask(self, energy, interval_num):
        """
        Override
        """
        return 1 * energy

class RandomMarket(implements(EnergyMarket)):
    """
    Market-Implementation for testing and analyzing
    """

    def __init__(self, size):
        self.__uniform_random_values = np.random.uniform(0, 3, size)

    def __get_value(self, energy, interval_num):
        return self.__uniform_random_values[interval_num] * energy

    @property
    def uniform_random_values(self):
        return self.__uniform_random_values

    @overrides
    def offer(self, energy, interval_num):
        """
        Override
        """
        return self.__get_value(energy, interval_num)

    @overrides
    def ask(self, energy, interval_num):
        """
        Override
        """
        return self.__get_value(energy, interval_num)


class UserMarket(implements(EnergyMarket)):
    """
    Modeling User-Market, which wraps a normal EnergyMarket. It delegates to its
    wrapper but increases the price the user has to pay enormously.
    """

    def __init__(self, wrapping_energy_market):
        self.__energy_market = wrapping_energy_market

    @overrides
    def offer(self, energy, interval_num):
        """
        Override
        """
        return self.__energy_market.offer(energy, interval_num)

    @overrides
    def ask(self, energy, interval_num):
        """
        Override
        """
        return self.__energy_market.ask(energy, interval_num) * 2


class ListMarket(implements(EnergyMarket)):
    """
    Energy-Market based on a python list.
    """

    def __init__(self, energy_values_offer, energy_values_ask):
        self.__energy_values_offer = energy_values_offer
        self.__energy_values_ask = energy_values_ask

    @overrides
    def offer(self, energy, interval_num):
        """
        Override
        """
        return self.__energy_values_offer[interval_num] * energy

    @overrides
    def ask(self, energy, interval_num):
        """
        Override
        """
        return self.__energy_values_ask[interval_num] * energy