"""
Configuration for the EO-COHDA Unit-Models, including the paramter for the
algorithm, the market, the storage, the participating agents
"""
import os
import copy

from eocohda.core.storage import EnergyStorage
from eocohda.core.evaluator import EvaluatorModel
from eocohda.core.market import RandomMarket
from eocohda.algorithm.evo import EvoAlgorithm, EvoAlgorithmBlackbox, EvoAlgorithmParetoNSGA

class CohdaConfig:

    # path to project
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(".")))

    # path to data
    DATA_PATH = os.path.join(PROJECT_PATH, 'data')

    # path to result folder
    RESULT_PATH = os.path.join(PROJECT_PATH, 'results')

    # path to flexibility folder
    FLEX_PATH = os.path.join(DATA_PATH, 'DER_schedules')

    # path to db-file
    DB_FILE = os.path.join(RESULT_PATH, 'results.hdf5')

    # time zone
    LOCAL_TZ = 'Europe/Berlin'

    # optional configuration for debugging clock
    CLOCK = {
        'start': '2016-04-01T00:30:00+02:00',
        'stop': '2016-07-31T23:30:00+02:00',
        'speed_up': 100,
    }

    INTERVAL_NUMBER = 96

    ENERGY_STORAGE = EnergyStorage.builder() \
        .step_size(1) \
        .load(0.5) \
        .self_discharge(0.000000001) \
        .capacity(50000) \
        .charge_efficiency(0.95) \
        .discharge_efficiency(0.95) \
        .max_charge(50000) \
        .max_discharge(50000) \
        .build()

    EVALUATOR = EvaluatorModel(ENERGY_STORAGE, RandomMarket(INTERVAL_NUMBER))

    ENERGY_STORAGE_ALGORITHM = EvoAlgorithm(20, 10, 8, 1000, EVALUATOR)

    ENERGY_STORAGE_ALGORITHM_PARETO = EvoAlgorithmParetoNSGA(20, 10, 8, 1000, EVALUATOR)

    ENERGY_STORAGE_ALGORITHM_BLACK_BOX = EvoAlgorithmBlackbox(20, 10, 8, 1000, EVALUATOR)

    UNIT_AGENT_CONFIG = [
        {
            'name' : 'Energiespeicher A'
        },
        {
            'name' : 'Energiespeicher B'
        },
        {
            'name' : 'Energiespeicher C'
        }
    ]

    UNIT_MODEL_CONFIG_LIST = [
        {
            'algorithm' : ENERGY_STORAGE_ALGORITHM,
            'storage' : ENERGY_STORAGE
        },
        {
            'algorithm' : ENERGY_STORAGE_ALGORITHM_PARETO,
            'storage' : ENERGY_STORAGE
        },
        {
            'algorithm' : ENERGY_STORAGE_ALGORITHM_BLACK_BOX,
            'storage' : ENERGY_STORAGE
        }
    ]

    UNIT_MODEL_LIST = [
        'eocohda.core.core:EnergyStorageUnitModel',
        'eocohda.core.core:EnergyStorageUnitModel',
        'eocohda.core.core:EnergyStorageUnitModel'
    ]

    # controller and observer configs
    CTRL_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'negotiation_single_start': True,
        'negotiation_timeout': 15*60,           # seconds
        'topology_manager': 'eocohda.isaac.controller.core.management:TopologyManager',
        'topology_phi': 1,          # We'll get a ring topology plus *at most* n_agents * phi connections.
        'topology_seed': None,      # random seed used for topology creation
        'scheduling_res': 15 * 60,  # resolution in seconds
        'scheduling_period': INTERVAL_NUMBER * 15 * 60   # one day
    }

    OBS_CONFIG = {
        'n_agents': len(UNIT_MODEL_LIST),
        'log_dbcls': 'eocohda.isaac.observer.core.monitoring:Monitoring',
        'log_dbfile': DB_FILE,
        'termcls': 'eocohda.isaac.observer.core.termination:MessageCounter'
    }

    PLANNER_CLS = 'eocohda.isaac.core.planning:Planner'
    PLANNER_CONFIG = {'check_inbox_interval': .1}

    # negotiations
    NEGOTIATIONS = (
                    {
                        'date': '2017-07-05T00:00:00+00:00',
                        'target': os.path.join(DATA_PATH, 'targets', 'electrical_target1.csv')
                    },
                    {
                        'date': '2017-07-06T00:00:00+00:00',
                        'target': os.path.join(DATA_PATH, 'targets', 'electrical_target2.csv')
                    }
    )

    # container configs
    CTRL_OBS_CONTAINER = {'host': 'localhost', 'port': 5555}
    # container for unit agents
    AGENT_CONTAINER = [
                        {'host': 'localhost', 'port': 5556},
                        {'host': 'localhost', 'port': 5557}
    ]