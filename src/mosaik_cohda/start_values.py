from dataclasses import dataclass
from mango.messages.codecs import json_serializable


@json_serializable
@dataclass
class StartValues:
    schedule: list#[float]
    participants: list

@json_serializable
@dataclass
class SolutionSchedule:
    schedule: list#[float]

@json_serializable
@dataclass
class FinalTermination:
    terminated: bool
