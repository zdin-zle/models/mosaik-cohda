"""
Implementation of the `mosaik API`_ for the Pickle sim.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/high-level.html

"""
import mosaik_api
import pickle

from ..des_flex.flex_class import Flexibility


# The meta data that the "init()" call will return.
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'PickleSim': {
            'public': True,
            'params': [],
            'attrs': [
                'output',  # output
            ],
        },
    },
}


class PickleSim(mosaik_api.Simulator):
    """This class implements the mosaik API."""
    def __init__(self):
        # We need to pass the META data to the parent class which will extend
        # it and store it as "self.meta":
        super().__init__(META)

        self.pickle_file = None  # File handle for wind velocities
        self.pickles = {}
        self.pickle_config = []
        self.object = {}
        self.eid_ = []
        self.cache = {}
        self.step_size = 3600

    def init(self, sid, step_size=3600, time_resolution=1., **sim_params):
        """*pickle_file* is a pickle file
        """
        if step_size not in [15*60, 60*60]:
            raise ValueError(f"Only a step size of 1h (3600s) or 15min (900s) is allowed. Instead, {step_size} was given")

        self.step_size=step_size

        pickle_files = sim_params['pickle_files']
        if "duration" in sim_params:
            duration = sim_params['duration']
        else: 
            with open(pickle_files[0], "rb") as pickle_file:
                temp_flex = pickle.load(pickle_file)
            
            duration = len(temp_flex['flex_max_power'])

        for idx, val in pickle_files.items():
            pickle_file = val
            with open(pickle_file, "rb") as pickle_flex_data:
                loaded_flex = pickle.load(pickle_flex_data)

            self.object[idx] = Flexibility(
                flex_max_power=loaded_flex['flex_max_power'][0:duration],
                flex_min_power=loaded_flex['flex_min_power'][0:duration],
                flex_max_energy_delta=loaded_flex['flex_max_energy_delta'][0:duration],
                flex_min_energy_delta=loaded_flex['flex_min_energy_delta'][0:duration]
            )

        # Return our simulator's meta data to mosaik:
        return self.meta

    def create(self, num, model, **pickles_params):
        """Create *num* instances of *model*.

        Mosaik makes sure that we get valid values for *num* and *model*.
        Since we only exposed one model, we know that *model* is "flex".


        """
        n_pickle = len(self.pickles)  # Number of pickle-simulators so far
        entities = []  # This will hold the entity data for mosaik
        
        for pickle_idx in range(n_pickle, n_pickle + num):
            # The entity ID for mosaik:
            eid = 'pickels-%s' % pickle_idx
            self.eid_.append(eid)
            self.cache[eid]=self.object[pickle_idx]
            self.pickles[eid] = pickle_idx

            # Add entity data for mosaik
            entities.append({'eid': eid, 'type': model})

        # Return the list of entities to mosaik:
        return entities

    def setup_done(self):
        """Called once the scenario creation is done and just before the
        simulation starts.

        """

    def step(self, time, inputs, max_advance):
        """Step the simulator ahead in time.  The current simulation time is
        *time*.

        *inputs* may contain *P_Max* values set by the MAS.  It is a dict
        like::


        """
        # No Computation, output stays the same for all steps

        # We want to do our next step in STEP_SIZE in seconds:
        return time + self.step_size

    def get_data(self, outputs):
        """
            Return the data requested by mosaik (only output).  *outputs* is a
            dict like::


        Return a dict with the requested data::

            {
                'pickle-0': {
                    'output': object,
                },
                ...
            }

        """
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.pickles:
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                if attr not in self.meta['models']['PickleSim']['attrs']:
                    raise AttributeError('Attribute "%s" not available' % attr)
                data[eid][attr] = self.cache[eid]


        return data


def main():
    """Run our simulator and expose the "PickleSim"."""
    return mosaik_api.start_simulation(PickleSim(), 'Pickle simulator')
