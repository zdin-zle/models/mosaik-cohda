"""
Implementation of the `mosaik API`_ for the Pickle sim.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/high-level.html
"""

import pickle
import mosaik_api
from ..start_values import StartValues


META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'OpPlanSim': {
            'public': True,
            'params': [],
            'attrs': [
                'StartValues',  # output
                'OutputValue',  # output value for each time step
            ],
        },
    },
}


class OpPlanSim(mosaik_api.Simulator):
    """Class implementing the mosaik API."""
    def __init__(self):
        """
        Initialize OpPlanSim class.
        """
        super().__init__(META)
        self.op_plan = {}
        self.object: StartValues
        self.step_size = 15*60
        self.cache_dict={}
        self.step_counter=0

    def init(self, sid, step_size=15*60, time_resolution=1., **sim_params):
        """
        Initialize the simulator and create Operational Plan object.

        Parameters:
        -----------
        sid : str
            Simulation instance ID.
        step_size : int, optional
            Time step size for simulation (default is 900).
        time_resolution : float, optional
            Time resolution for simulation (default is 1.0).
        **sim_params : dict
            Additional simulation parameters.

        Returns:
        --------
        dict
            Meta information about the simulator.
        """
        if step_size not in [15*60, 60*60]:
            raise ValueError(f"Only a step size of 1h (3600s) or 15min (900s) is allowed. Instead, {step_size} was given")

        self.step_size = step_size
        if 'pickle_file' not in sim_params.keys():
            raise ValueError("Parameter 'pickle_file' must be given in the 'sim_params'")
        pickle_file = sim_params['pickle_file']
        self.pickle_file = pickle_file
        duration = sim_params.get("duration", None)
        loaded_flex = pickle.load(open(self.pickle_file, 'rb'))
        schedule = loaded_flex["schedule"][0:duration] if duration else loaded_flex["schedule"]
        participants = sim_params["participants"]

        self.object = StartValues(schedule=schedule, participants=participants)
        self.cache_dict["schedule"]=schedule
        return self.meta

    def create(self, num, model):
        """
        Create instances of a specified model.

        Parameters:
        -----------
        num : int
            Number of model instances to create.
        model : str
            Type of model to create.

        Returns:
        --------
        list
            List of entities created.
        """
        if num > 1:
            raise ValueError("Only one model per simulator can be created")

        existing_instances = len(self.op_plan)
        entities = []
        for idx in range(existing_instances, existing_instances + num):
            eid = 'op_plan-%s' % idx
            self.op_plan[eid] = idx
            entities.append({'eid': eid, 'type': model})

        return entities

    def setup_done(self):
        """
        Execute actions after scenario creation and before simulation starts.
        """
        pass  # Placeholder for setup actions if needed

    def step(self, time, inputs, max_advance):
        """
        Advance the simulator in time.

        Parameters:
        -----------
        time : int
            Current simulation time.
        inputs : dict
            Input values provided by the MAS.
        max_advance : int
            Maximum time to advance.

        Returns:
        --------
        int
            Time after stepping.
        """
        self.step_counter+=1
        return time + self.step_size

    def get_data(self, outputs):
        """
        Get data requested by mosaik.

        Parameters:
        -----------
        outputs : dict
            Requested output attributes.

        Returns:
        --------
        dict
            Data corresponding to requested outputs.
        """
        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.op_plan:
                raise ValueError('Unknown entity ID "%s"' % eid)

            data[eid] = {}
            for attr in attrs:
                if attr not in self.meta['models']['OpPlanSim']['attrs']:
                    raise AttributeError('Attribute "%s" not available' % attr)
                if attr == 'StartValues':
                    data[eid][attr] = self.object
                if attr == 'OutputValue':
                    data[eid][attr] = self.cache_dict["schedule"][self.step_counter-1]

        return data


def main():
    """
    Run the simulator and expose the "OpPlanSim".
    """
    return mosaik_api.start_simulation(OpPlanSim(), 'Operational Plan simulator')

if __name__ == "__main__":
    main()