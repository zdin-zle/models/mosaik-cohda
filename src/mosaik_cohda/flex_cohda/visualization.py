from ..des_flex.flex_class import Flexibility
import matplotlib.pyplot as plt

import numpy as np


def visualize_flex_schedules(flex: Flexibility, schedules, time_resolution):
    visualize_flex_energy_schedules(flex, schedules, time_resolution)
    visualize_flex_power_schedules(flex, schedules, time_resolution)
    plt.show()


def visualize_flex_power_schedules(flex: Flexibility, schedules,
                                   time_resolution):
    visualize_schedules_limit(flex.flex_max_power, flex.flex_min_power,
                              schedules, time_resolution, 'Power Schedule')


def visualize_flex_energy_schedules(flex: Flexibility, schedules,
                                    time_resolution):
    energy_schedules = convert_power_schedules_to_energy(schedules,
                                                         time_resolution)

    visualize_schedules_limit(flex.flex_max_energy_delta,
                              flex.flex_min_energy_delta,
                              energy_schedules, time_resolution, 'Energy '
                                                                 'Delta '
                                                                 'schedule')


def visualize_schedules_limit(upper_limit, lower_limit, schedules,
                              time_resolution, y_label):
    plt.figure(figsize=(15, 5))
    width = 1
    t_max = len(upper_limit)
    x_max = t_max - 1
    x_ticks_count = 20
    ticks_font_size = 10
    label_font_size = 14

    t_steps = np.arange(len(upper_limit)) + 0.5
    y_max = max(1.3 * max(upper_limit), -0.3 * (min(upper_limit)))
    y_min = min(1.4 * min(lower_limit), -0.4 * (max(lower_limit)))
    plt.bar(t_steps, upper_limit, width, color='blue')

    # Divide lower limit
    lower_limit_positive = [x if x > 0 else 0 for x in lower_limit]
    lower_limit_negative = [x if x < 0 else 0 for x in lower_limit]
    plt.bar(t_steps, lower_limit_positive, width, color='white')
    plt.bar(t_steps, lower_limit_negative, width, color='blue')

    for schedule in schedules:
        plt.step(t_steps, schedule, linewidth=2, where='mid', color='k')

    plt.xlim([0, t_max])
    plt.xticks(np.linspace(0, x_max, x_ticks_count),
               fontsize=ticks_font_size)
    plt.xlabel('Time steps à 15 minutes', fontsize=label_font_size)
    plt.ylim([y_min, y_max])
    plt.ylabel(y_label, fontsize=label_font_size)

    plt.grid(which='both', color='0.5', linestyle='-',
             linewidth=0.5)
    plt.tight_layout()

    return None


def convert_power_schedules_to_energy(schedules, time_resolution):
    number_schedules = len(schedules)
    length_schedule = len(schedules[0])

    energy_schedules = []

    for i in range(number_schedules):
        delta_energy = 0
        energy_schedule = np.zeros(length_schedule)
        for j in range(length_schedule):
            delta_energy = delta_energy + schedules[i][j] * \
                           time_resolution / 3600
            energy_schedule[j] = delta_energy
        energy_schedules.append(energy_schedule)

    return energy_schedules
