from overrides import overrides
from ..eocohda.core.storage import EnergyStorage
from ..des_flex.flex_class import Flexibility


class FlexStorage(EnergyStorage):

    def __init__(self, flex: Flexibility, time_resolution: int):
        """
        Initializes the model with some parameters.

        :param flex: The flexibility contains the boundaries for the schedules
        :param time_resolution: time resolution for flex (relevant to switch
        between power and energy
        """

        # New values
        self.__flexibility = flex
        self.__power_to_energy_factor = time_resolution / 3600
        self.__soc_to_energy_factor = (max(flex.flex_max_energy_delta) - min(
            flex.flex_min_energy_delta))  # Factor to shrink the
        # energy band into a soc-band

        # Values from EnergyStorage
        self._step = 0
        self._soc = 0.5  # Virtual SOC limited by 0 and 1
        self._time_constant = time_resolution  # in seconds per interval
        self._max_discharge = min(self.__flexibility.flex_min_power)
        self._max_charge = max(self.__flexibility.flex_max_power)

    def copy(self):
        storage = FlexStorage(self.__flexibility, self._time_constant)
        storage.load = self.load
        return storage

    @property
    def operating_cost(self):
        """
        :returns: cost-horizon, operating-cost in the planning_horizon
        """
        return 0

    def max_discharge_at(self, i):
        """
        :returns: max discharge power in W
        """
        return -self.__flexibility.flex_min_power[i]

    def max_charge_at(self, i):
        """
        :returns: max charge power in W
        """
        return self.__flexibility.flex_max_power[i]

    def max_allowed_soc_at(self, i):
        """
        :returns: max allowed SOC at i
        """

        return self.__flexibility.flex_max_energy_delta[
                   i] / self.__soc_to_energy_factor + 0.5

    def min_allowed_soc_at(self, i):
        """
        :returns: min allowed SOC at i
        """

        return self.__flexibility.flex_min_energy_delta[
                   i] / self.__soc_to_energy_factor + 0.5

    def load_to_energy_factor(self):
        return self.__soc_to_energy_factor

    def time_constant(self):
        return self._time_constant

    @overrides
    def _calc_energy(self, new_load):
        """
        Calc power for the state change to new_load

        :param new_load: new load of storage
        :returns: power required for state change
        """

        return (new_load - self._soc) * self.__soc_to_energy_factor / \
               self.__power_to_energy_factor

    @overrides
    def _calc_next(self, energy, is_charge):
        """
        Calculates next state of the model when charging/discharging an
        amount of <code>energy</code> to the battery. Therefore,
        there exists the <code>is_charge</code>-Flag.

        :param energy: power level for the interval
        :param is_charge: indicates whether you want to charge/discharge
        energy to the storage
        :returns: new load after charging/discharging the amount
        """

        return self._soc + energy * self.__power_to_energy_factor / \
               self.__soc_to_energy_factor

    def remaining_load_at(self, i):
        """
        Calculates the remaining load for discharge

        :returns: max allowed power in this time step (i) in W
        """
        delta_energy_of_load = (self._soc - 0.5) * self.__soc_to_energy_factor

        return -(self.__flexibility.flex_min_energy_delta[i] -
                 delta_energy_of_load) / self.__power_to_energy_factor

    def load_until_cap_at(self, i):
        """
        Calculates the load in W until the cap is reached

        :returns: necessary power in this step (i) to reach cap in W
        """
        delta_energy_of_load = (self._soc - 0.5) * self.__soc_to_energy_factor

        return (self.__flexibility.flex_max_energy_delta[i] -
                delta_energy_of_load) / self.__power_to_energy_factor

    def possible_load_one_step_at(self, i):
        """
        Calculates max possible load after this step (i) for charging and
        discharging

        :returns: tuple of load state (min_load_after_discharge,
        max_load_after_charge)
        """

        return [max(self.min_allowed_soc_at(i),
                    self._calc_next(-self.max_discharge_at(i), False)),
                min(self.max_allowed_soc_at(i),
                    self._calc_next(self.max_charge_at(i), True))]

    def convert_candidate_to_schedule(self, candidate):
        length_schedule = len(candidate.slots)
        schedule = []
        load_before = 0.5
        slots = candidate.slots
        for j in range(length_schedule):
            power = (slots[j].load_state - load_before) * \
                    self.__soc_to_energy_factor / self.__power_to_energy_factor
            load_before = slots[j].load_state
            schedule.append(power)

        return schedule
