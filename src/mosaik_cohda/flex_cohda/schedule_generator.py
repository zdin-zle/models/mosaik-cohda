from ..des_flex.flex_class import Flexibility
from ..eocohda.algorithm.evo import EvoAlgorithm

from .flexstorage import FlexStorage
from .evaluator import FlexEvaluator, FlexCohdaEvaluatorPlugin
from .solution_repairer import FlexSolutionRepairer


class ScheduleGenerator:
    def __init__(self, *, flexibility: Flexibility, time_resolution=60 * 15):
        self.__flex = flexibility
        assert len(self.__flex.flex_max_power) == \
               len(self.__flex.flex_min_power) == \
               len(self.__flex.flex_max_energy_delta) == \
               len(self.__flex.flex_min_energy_delta)

        self.__time_resolution = time_resolution  # minutes per interval
        self.__cohda_objective = lambda solution: -999999
        self.__interval_length = len(self.__flex.flex_max_power)
        self.__start = 0

        # Init model:
        self.__update_model(population_size=10, gen_size=10, iteration_count=1)

    def update_flexibility(self, flexibility: Flexibility):
        self.__flex = flexibility
        self.__update_model()

    def update_cohda_objective(self, cohda_objective):
        self.__cohda_objective = cohda_objective
        self.__update_model(population_size=16, gen_size=8,
                            iteration_count=100)

    def __update_model(self, population_size=30, gen_size=30,
                       iteration_count=500):
        # build storage
        self.__storage = FlexStorage(self.__flex, self.__time_resolution)
        # Create evaluator plugin
        self.__flex_cohda_evaluator_plugin = \
            FlexCohdaEvaluatorPlugin(self.__storage, self.__cohda_objective)
        # Create evaluator
        self.__evaluator = \
            FlexEvaluator(
                self.__storage,
                evaluator_plugins=[self.__flex_cohda_evaluator_plugin])
        # Create algorithm
        self.__algorithm = EvoAlgorithm(self.__evaluator,
                                        solution_size=population_size,
                                        gen_size=gen_size, parent_num=2,
                                        iteration_count=iteration_count)
        # Create solution repairer and set custom repairer
        flex_solution_repairer = FlexSolutionRepairer(self.__evaluator)
        self.__algorithm.repairer = flex_solution_repairer

    def create_schedules(self):
        # Start algorithm to generate schedules
        self.__algorithm.generate_schedules(self.__start, 1,
                                            self.__interval_length, 1)
        # Get history of solutions as init for next iteration
        self.__start = self.__algorithm.pop_history[-1]

        # Get solutions as output
        solutions = self.__algorithm.best_history

        # Convert solutions to schedules
        schedules = self.__convert_solutions_to_schedules(solutions)

        return schedules

    def current_solutions(self):
        if self.__algorithm.best_history is not None:
            return self.__algorithm.best_history
        return []

    def __convert_solutions_to_schedules(self, solutions):
        # Get length
        number_solution = len(solutions)
        schedules = []

        # Loop over solutions and over solution steps
        for i in range(number_solution):
            schedules.append(self.__storage.convert_candidate_to_schedule(
                solutions[i].candidate))

        return schedules
