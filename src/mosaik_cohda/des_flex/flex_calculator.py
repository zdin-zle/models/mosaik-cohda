import numpy as np
import matplotlib.pyplot as plt

from typing import List, Tuple, NamedTuple, Optional

from .flex_class import Flexibility, ExtendedFlexibility
from .flex_visualization import plot_limits



'''
Flexibility is calculated depending on the optimal operation plans of
all electric devices and their unique parameters.
'''

class FlexCalculator:
    """
    Flex calculator is able to calculate all the 4 steps necessary for 
    the interval flexibility calculation.
    """

    def __init__(self, *, max_p_stor: int, min_p_stor: int, capacity_energy: int,
                 efficiency_charge: float, efficiency_discharge: float,
                 soh: float = 1):
        """
        :param max_p_stor: Maximum power of the storage (>0) [W]
        :param min_p_stor: Minimum power of the storage (<0) [W]
        :param capacity_energy: Capacity of the storage [Wh]
        :param efficiency_charge: Efficiency of charging [0,1]
        :param efficiency_discharge: Efficiency of discharging [0,1]
        :param soh: The state of health of the storage [0,1]. It reduces
        the usable capacity of the storage
        """
        self.max_p_stor = max_p_stor                          # [W]
        self.min_p_stor = min_p_stor                          # [W]
        self.nominal_capacity = capacity_energy               # [W]
        self.capacity_energy = capacity_energy * soh          # [Wh]
        self.efficiency_charge = efficiency_charge            # [0,1]
        self.efficiency_discharge = efficiency_discharge      # [0,1]
        self.soh = soh                                        # [0,1]

        # for one time step
        self.start_soc = 0.5                                   # [0,1]
        self.res = 60 * 60  # TOOD input Parameter
        self.absolute_flexibility = None
        self.relative_flexibility = None


    def update(self, *, max_p_stor: int, min_p_stor: int,
               soh: float):
        """
        Updates the flex calculator
        :param max_p_stor: Maximum power of the storage (>0) [W]
        :param min_p_stor: Minimum power of the storage (<0) [W]
        :param soh: The state of health of the storage [0,1]
        """
        self.max_p_stor = max_p_stor
        self.min_p_stor = min_p_stor
        self.soh = soh
        # adapt capacity due to SoH
        self.capacity_energy = soh * self.nominal_capacity


    def power_to_soc_diff(self, *, power: int, relative: bool = False) \
            -> float:
        """
        Calculates the difference in SoC according to the given external power
        value over time considering efficiency
        :param power: the power value [W]
        :param relative: true for relative flex calculation, where the power
        input already includes the efficiencies
        :return: the change in SoC [-1, 1]
        """
        if relative:
            # for relative flexibility, the efficiency cannot be included here
            power_at_stor = power
        elif power > 0:
            # charging is less than the external power
            power_at_stor = power * self.efficiency_charge  # [W]
        else:
            # discharging is greater than the external power
            power_at_stor = power / self.efficiency_discharge  # [W]

        # energy difference is power * duration / one hour
        energy_diff = power_at_stor * self.res / (60 * 60)  # [Wh]
        soc_diff = energy_diff / self.capacity_energy  # [-1,1]

        return soc_diff


    def soc_diff_to_power(self, *, soc_diff: float, p_plan: float=0) \
            -> int:
        """
        Calculates the expected external power value given a change in the
        SoC and a time period
        :param soc_diff: the difference in the soc [-1, 1]
        :param p_plan: planned power value for relative flexibility

        :return: the external power value [W]
        """
        power = soc_diff * self.capacity_energy* 60 * 60 / self.res  # [W]
        if (power+p_plan) > 0:
            total_power = power / self.efficiency_charge  # [Wh]
        else:
            total_power = power * self.efficiency_discharge  # [Wh]

        return total_power

    def calculate_available_power_range(self, *,
                                        forecast: List[Optional[float]],
                                        p_max_ps: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        Step 1
        Calculates the available power range (min of power diff and p_max).
        The power in the current interval respects the already realized storage
        power.
        :param forecast: the load forecast [W]
        :param p_max_ps: the p_max for peak shaving per interval [W]
        :return: power range max and power range min
        """
        # synchronize lengths of vectors from forecast, peak shaving power
        assert len(forecast) == len(p_max_ps)

        power_diff = [x - y if y is not None else None
                      for x, y in zip(p_max_ps, forecast)]
        # get power diff
        # power range for p_max is min of power diff and physical constraint
        available_max_power = [min(x, self.max_p_stor) if x is not None
                               else self.max_p_stor for x in power_diff]
        # power range is determined by physical constraint
        available_min_power = [self.min_p_stor] * len(power_diff)


        return available_max_power, available_min_power

    def calculate_soc_flexibility_backward(
            self, *, available_max_power: List[float],
            available_min_power: List[float],
            final_min_soc: float = 0., final_max_soc: float = 1.,
            planned_soc_change_max: List[float] = None,
            planned_soc_change_min: List[float] = None,
            relative: bool = False) -> Tuple[List[float], List[float]]:
        """
        Step 2b: Calculate the required SoC range for the end of each
        interval
        ATTENTION: This has changed to the end of the interval
        :param available_max_power: the maximum available power
        calculated in step 1 [W]
        :param available_min_power: the minimum available power
        calculated in step 1 [W]
        :param final_min_soc: the required min SoC at the end of
        the planning horizon [0,1], defaults to 0
        :param final_max_soc: the required max SoC at the end of the
         planning horizon [0,1], defaults to 1
        :param planned_soc_change_max: the soc change resulting from the
        planned power schedule for relative flexibility in case of
        maximum power flexibility
        :param planned_soc_change_min: the soc change resulting from the
        planned power schedule for relative flexibility in case of
        minimum power flexibility
        :param relative: True, if relative flexibility is calculated
        :return: max_required_soc and min_required_soc for the end of each
        interval
        """
        # Check lengths
        assert len(available_max_power) == len(available_min_power)

        if relative:
            assert len(available_max_power) == len(planned_soc_change_max) \
                   == len(planned_soc_change_min)

        # initialize return lists (-1. is just a placeholder)
        period = len(available_max_power)
        required_max_soc = [-1.] * period
        required_min_soc = [-1.] * period

        if relative:
            max_soc_at_interval_start = \
                min(self.absolute_flexibility.flex_max_soc[period - 1] -
                    planned_soc_change_min[period - 1],
                    self.absolute_flexibility.flex_max_soc[period - 1] -
                    planned_soc_change_max[period - 1])
            min_soc_at_interval_start = \
                max(self.absolute_flexibility.flex_min_soc[period - 1] -
                    planned_soc_change_max[period - 1],
                    self.absolute_flexibility.flex_min_soc[period - 1] -
                    planned_soc_change_min[period - 1])
        else:
            max_soc_at_interval_start = final_max_soc
            min_soc_at_interval_start = final_min_soc

        required_max_soc[period - 1] = max_soc_at_interval_start
        required_min_soc[period - 1] = min_soc_at_interval_start

        # starts at the last interval, until interval 1
        for interval_no in range(period - 1, 0, -1):
            # required start soc values of next interval are required end
            # soc values for this interval
            max_soc_at_interval_end = max_soc_at_interval_start
            min_soc_at_interval_end = min_soc_at_interval_start

            # get maximum soc change on the basis of the available max power
            # (can be < 0 if discharging is required)
            max_soc_change = self.power_to_soc_diff(
                power=available_max_power[interval_no],
                relative=relative
            )
            # get minimum soc change on the basis of the available min power
            # (can be > 0 if charging is required)
            min_soc_change = self.power_to_soc_diff(
                power=available_min_power[interval_no],
                relative=relative
            )

            if relative:
                max_soc_at_interval_start = \
                    min(self.absolute_flexibility.flex_max_soc[interval_no -
                                                               1] -
                        planned_soc_change_min[interval_no - 1],
                        max_soc_at_interval_end - min_soc_change)
                min_soc_at_interval_start = \
                    max(self.absolute_flexibility.flex_min_soc[interval_no -
                                                               1] -
                        planned_soc_change_max[interval_no - 1],
                        min_soc_at_interval_end - max_soc_change)
            else:
                # get max soc at interval start (cannot be > 1)
                max_soc_at_interval_start = min(1.0, max_soc_at_interval_end -
                                            min_soc_change)

                # get min soc at interval start (cannot be < 0)
                min_soc_at_interval_start = max(0.0, min_soc_at_interval_end -
                                            max_soc_change)

                if max_soc_at_interval_start < 0:
                    # no problem detection here so min is 0
                    max_soc_at_interval_start = 0

                if min_soc_at_interval_start > 1:
                    # no problem detection here so max is 1
                    min_soc_at_interval_start = 1

            # the required at the end of last interval must be this start
            required_max_soc[interval_no - 1] = max_soc_at_interval_start
            required_min_soc[interval_no - 1] = min_soc_at_interval_start

        return required_max_soc, required_min_soc

    def calculate_soc_flexibility_forward(
            self, *, available_max_power: List[float],
            available_min_power: List[float],
            planned_soc_change_max: List[float] = None,
            planned_soc_change_min: List[float] = None,
            relative: bool = False) \
            -> Tuple[List[float], List[float]]:
        """
        Step 2a: Calculate the reachable SoC range at the end of each
        interval
        :param available_max_power: the maximum available power
        calculated in step 1 [W]
        :param available_min_power: the minimum available power
        calculated in step 1 [W]
        :param planned_soc_change_max: the soc change resulting from the
        planned power schedule for relative flexibility in case of
        maximum power flexibility
        :param planned_soc_change_min: the soc change resulting from the
        planned power schedule for relative flexibility in case of
        minimum power flexibility
        :param relative: True, if relative flexibility is calculated
        :return: the reachable SoC at the beginning of each interval and
        the min of max_soc and the max of min_soc within the critical period
        """
        # Check lengths
        assert len(available_max_power) == len(available_min_power)
        period = len(available_min_power)

        reachable_max_soc = [-1.] * period
        reachable_min_soc = [-1.] * period

        # Minimum and maximum are defined by soc_start
        min_soc_at_interval_end = self.start_soc
        max_soc_at_interval_end = self.start_soc

        for current_interval in range(period):
            # start values are the end values of last interval
            min_soc_at_interval_start = min_soc_at_interval_end
            max_soc_at_interval_start = max_soc_at_interval_end

            # get maximum soc change on the basis of the available max power
            # (can be < 0 if discharging is required)
            max_soc_change = self.power_to_soc_diff(
                power=available_max_power[current_interval],
                relative=relative
            )
            # print('max_soc_change: '+str(max_soc_change))
            # get minimum soc change on the basis of the available min power
            # (can be > 0 if charging is required)
            min_soc_change = self.power_to_soc_diff(
                power=available_min_power[current_interval],
                relative=relative
            )
            # print('min_soc_change: '+str(min_soc_change))

            if relative:
                max_soc_at_interval_end = \
                    min(self.absolute_flexibility.flex_max_soc[
                            current_interval] -
                        planned_soc_change_max[current_interval],
                        max_soc_at_interval_start + max_soc_change)
                min_soc_at_interval_end = \
                    max(self.absolute_flexibility.flex_min_soc[
                            current_interval] -
                        planned_soc_change_min[current_interval],
                        min_soc_at_interval_start + min_soc_change)
            else:
                max_soc_at_interval_end = min(1.0, max_soc_at_interval_start +
                                          max_soc_change)
                min_soc_at_interval_end = max(0.0, min_soc_at_interval_start +
                                          min_soc_change)

            reachable_min_soc[current_interval] = min_soc_at_interval_end
            reachable_max_soc[current_interval] = max_soc_at_interval_end

        return reachable_max_soc, reachable_min_soc

    @staticmethod
    def calculate_allowed_soc_range(
            *, required_max_soc: List[float],
            required_min_soc: List[float], reachable_max_soc: List[float],
            reachable_min_soc: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        Step 2c: Calculate the allowed soc range at the end of each interval
        :param required_max_soc: Required max socs (calculated in step 2b)
        :param required_min_soc: Required min socs (calculated in step 2b)
        :param reachable_max_soc: Reachable max socs (calculated in step 2a)
        :param reachable_min_soc: Reachable min socs (calculated in step 2a)
        :return: allowed_max_soc, allowed_min_soc
        """

        # all lists have to have the same length
        assert len(required_max_soc) == len(required_min_soc) == \
               len(reachable_max_soc) == len(reachable_min_soc),\
            f'Length of lists from step 2 and 3 are not equal, ' \
            f'len of required is {len(required_max_soc)} ' \
            f'and len of reachable is {len(reachable_max_soc)}'


        period = len(required_min_soc)

        allowed_min_soc = []
        allowed_max_soc = []

        for current_interval in range(period):
            # remove problems, in which case these intervals do not
            # overlap! Possible in case of very large, unshavable peaks
            if required_max_soc[current_interval] < \
                reachable_min_soc[current_interval]:
                required_max_soc[current_interval] = \
                    reachable_min_soc[current_interval]
            if required_min_soc[current_interval] > \
                reachable_max_soc[current_interval]:
                required_min_soc[current_interval] = \
                    reachable_max_soc[current_interval]

            # min SoC is max of min_required and min_reachable
            allowed_min_soc.append(
                max(required_min_soc[current_interval],
                    reachable_min_soc[current_interval])
            )

            # max SoC is min of max_required and max_reachable
            allowed_max_soc.append(
                min(required_max_soc[current_interval],
                    reachable_max_soc[current_interval])
            )

        return allowed_max_soc, allowed_min_soc

    def convert_soc_range_to_energy_flex(
            self, *, max_soc: List[float], min_soc: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        Step 3: Converts the allowed soc range to energy flexibility
        at the end of each interval, which
        will be communicated to the aggregator
        The result communicates the available energy flexibility at the END of
        each interval.
        :param max_soc: list of max soc values [0,1]
        :param min_soc: list of min soc values [0,1]
        :return: max_energy_delta, min_energy_delta
        """
        assert len(max_soc) == len(min_soc)
        max_energy_delta = []
        min_energy_delta = []

        for current_max_soc, current_min_soc in zip(max_soc, min_soc):
            max_energy_delta.append(
                (current_max_soc - self.start_soc) * self.capacity_energy)
            min_energy_delta.append(
                (current_min_soc - self.start_soc) * self.capacity_energy)

        return max_energy_delta, min_energy_delta


    def calculate_allowed_power_flexibility(
            self, *, allowed_max_soc: List[float],
            allowed_min_soc: List[float], available_max_power: List[float],
            available_min_power: List[float], p_plan: List[float]=None,
            relative: bool=False) \
            -> Tuple[List[float], List[float]]:
        """
        Step 4: calculates the allowed power range
        :param allowed_max_soc: Allowed max SoC calculated in step 3
        :param allowed_min_soc: Allowed min SoC calculated in step 3
        :param available_max_power: available max power calculated in step 1
        :param available_min_power: available min power calculated in step 1
        :param p_plan: planned power schedule for relative flexibility
        :param relative: True, if calculating relative flexibility
        :return: allowed_max_power, allowed_min_power
        """

        assert len(allowed_min_soc) == len(allowed_max_soc) == \
               len(available_min_power)  == len(available_max_power)

        period = len(available_max_power)

        allowed_max_power = []
        allowed_min_power = []

        for current_interval in range(period):
            if current_interval == 0:
                max_soc_diff = allowed_max_soc[current_interval] - \
                               self.start_soc
                min_soc_diff = allowed_min_soc[current_interval] - \
                               self.start_soc
            else:
                # get maximum and minimum SoC diff for this interval
                max_soc_diff = allowed_max_soc[current_interval] - \
                               allowed_min_soc[current_interval - 1]

                min_soc_diff = allowed_min_soc[current_interval] - \
                               allowed_max_soc[current_interval - 1]

            if relative:
                current_p_plan = p_plan[current_interval]
            else:
                current_p_plan = 0

            # convert SoC diff to power per interval
            max_power_from_soc = \
                self.soc_diff_to_power(soc_diff=max_soc_diff,
                                       p_plan=current_p_plan)
            min_power_from_soc = \
                self.soc_diff_to_power(soc_diff=min_soc_diff,
                                       p_plan=current_p_plan)

            # maximum allowed power is min of max_power_from_soc
            # and power_range_max
            allowed_max_power.append(
                min(max_power_from_soc, available_max_power[current_interval])
            )

            # minimum allowed power is max of min_power_from_soc
            # and power_range_min
            allowed_min_power.append(
                max(min_power_from_soc, available_min_power[current_interval]))

        return allowed_max_power, allowed_min_power

    def calculate_absolute_flex(self, *, forecast: List[float],
                             p_max_ps: List[float],
                             start_soc: float,
                             final_min_soc: float = 0.,
                             final_max_soc: float = 1.,
                             res: int = 15 * 60) \
            -> ExtendedFlexibility:
        """
        The absolute flexibility does not take any planned schedule into account.
        Performs all flex calculation steps and stores them in a namedtuple
        :param forecast: the load forecast [W]
        :param p_max_ps: the p_max for peak shaving per interval [W]
        :param start_soc: the current SoC of the storage [0,1]
        :param final_min_soc: the required min soc at the end [0,1]
        :param final_max_soc: the required max soc at the end [0,1]
        :param res: the resolution to use [s]
        :return: namedtuple Flexibility
        """

        # Set internal values
        if start_soc < 0 or start_soc > 1:
            print("Start soc (",start_soc,")needs to be within [0:1]")
            return None
        self.start_soc = start_soc
        self.res = res

        # Only Peak Shaving:
        # Step 1
        available_max_power, available_min_power = \
            self.calculate_available_power_range(
                forecast=forecast,
                p_max_ps=p_max_ps)

        # Step 2a calculate reachable SoC range
        reachable_max_soc, reachable_min_soc = \
            self.calculate_soc_flexibility_forward(
                available_max_power=available_max_power,
                available_min_power=available_min_power,
            )

        # Step 2b calculate required SoC range
        required_max_soc, required_min_soc = \
            self.calculate_soc_flexibility_backward(
                available_max_power=available_max_power,
                available_min_power=available_min_power,
                final_min_soc=final_min_soc,
                final_max_soc=final_max_soc,
            )

        # Step 2c combine 2a and 2b to allowed SoC range
        allowed_max_soc, allowed_min_soc = \
            self.calculate_allowed_soc_range(
                required_max_soc=required_max_soc,
                required_min_soc=required_min_soc,
                reachable_max_soc=reachable_max_soc,
                reachable_min_soc=reachable_min_soc
            )

        # Step 3 convert to energy flexibility/delta energy
        allowed_max_energy_delta, allowed_min_energy_delta = \
            self.convert_soc_range_to_energy_flex(
                max_soc=allowed_max_soc,
                min_soc=allowed_min_soc
            )

        # Step 4 calculate allowed power flexibility
        allowed_max_power, allowed_min_power = \
            self.calculate_allowed_power_flexibility(
                allowed_max_soc=allowed_max_soc,
                allowed_min_soc=allowed_min_soc,
                available_max_power=available_max_power,
                available_min_power=available_min_power,
            )

        # store result in named tuple
        self.absolute_flexibility = ExtendedFlexibility(
            flex_max_power=allowed_max_power,
            flex_min_power=allowed_min_power,
            flex_max_soc=allowed_max_soc,
            flex_min_soc=allowed_min_soc,
            max_plan_soc=np.zeros(len(allowed_max_soc)),
            min_plan_soc=np.zeros(len(allowed_max_soc)),
            flex_max_energy_delta=allowed_max_energy_delta,
            flex_min_energy_delta=allowed_min_energy_delta
        )

        return self.absolute_flexibility

    def check_planned_schedule(self, *, p_plan: List[float])\
            -> bool:
        """
         The function checks if the planned schedule lays within the allowed
         absolute flexibility.
         :param p_plan: planned schedule [W]
         :return: true, if the plan is valid; false, if otherwise
         """

        period = len(p_plan)
        soc_plan = np.zeros(period)

        time_capacity_factor = self.res / (60 * 60) / self.capacity_energy

        for i in range(period):
            # Check power limits
            if p_plan[i] > self.absolute_flexibility.flex_max_power[i] or \
                    p_plan[i] < self.absolute_flexibility.flex_min_power[i]:
                print(
                    "p_plan at ", i,
                    " is not within the allowed power band: p_plan: ",
                    p_plan[i], "; p_abs_max: ",
                    self.absolute_flexibility.flex_max_power[i],
                    "; p_abs_min: ",
                    self.absolute_flexibility.flex_min_power[i])
                return False

            if i == 0:
                soc_plan[i] = self.start_soc
            else:
                soc_plan[i] = soc_plan[i-1]

            if p_plan[i] > 0:
                soc_plan[i] += \
                    p_plan[i] * time_capacity_factor * self.efficiency_charge
            else:
                soc_plan[i] += \
                    p_plan[i] * time_capacity_factor * \
                    1/self.efficiency_discharge

            # Check SOC limits
            if soc_plan[i] > self.absolute_flexibility.flex_max_soc[i] or \
                    soc_plan[i] < self.absolute_flexibility.flex_min_soc[i]:
                print("p_plan: ",p_plan)
                print("soc_plan: ",soc_plan)
                print(
                    "p_plan at ", i," (", soc_plan[i],
                    ") is not within the allowed soc band:",
                    self.absolute_flexibility.flex_max_soc[i],
                    self.absolute_flexibility.flex_min_soc[i])
                return False

        return True

    def calculate_relative_power_flex(self, *, p_plan: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        By combining the planned schedule and the absolute power flexibility,
        a relative power flexibility can be calculated
        :param p_plan: planned schedule [W]
        :return: power_rel_max, power_rel_min: the upper and lower limit for
        relative power flexibility
        """

        period = len(p_plan)

        power_rel_max = np.zeros(period)
        power_rel_min = np.zeros(period)

        for i in range(len(p_plan)):
            power_rel_max[i] = \
                self.absolute_flexibility.flex_max_power[i]-p_plan[i]
            power_rel_min[i] = \
                self.absolute_flexibility.flex_min_power[i]-p_plan[i]

        return power_rel_max, power_rel_min

    def calculate_planned_soc_change(self, *, p_plan: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        The planned schedule causes a change in SOC calculated by this function.
        The function includes the results of efficiencies.
        :param p_plan: planned schedule [W]
        :return: soc_plan_max, soc_plan_min: the change of the SOC based on the
        maximal and minimal absolute power
        """

        period = len(p_plan)

        soc_plan_max = np.zeros(period)
        soc_plan_min = np.zeros(period)

        time_capacity_factor = self.res / (60 * 60) /self.capacity_energy

        for i in range(len(p_plan)):
            if i == 0:
                soc_plan_max[i] = 0
                soc_plan_min[i] = 0
            else:
                soc_plan_max[i] = soc_plan_max[i-1]
                soc_plan_min[i] = soc_plan_min[i-1]


            if p_plan[i] >= 0:
                soc_plan_max[i] += \
                    p_plan[i] * self.efficiency_charge * time_capacity_factor
            elif self.absolute_flexibility.flex_max_power[i] > 0:
                soc_plan_max[i] += p_plan[i] * time_capacity_factor
            else:
                soc_plan_max[i] += \
                    (p_plan[i]+self.absolute_flexibility.flex_max_power[i](1/
                    self.efficiency_discharge-1)) * time_capacity_factor

            if p_plan[i] <= 0:
                soc_plan_min[i] += \
                    p_plan[i] * 1/self.efficiency_discharge * \
                    time_capacity_factor
            elif self.absolute_flexibility.flex_min_power[i] < 0:
                soc_plan_min[i] += p_plan[i] * time_capacity_factor
            else:
                soc_plan_min[i] += \
                    (p_plan[i] + self.absolute_flexibility.flex_min_power[i](
                    self.efficiency_charge - 1)) * time_capacity_factor

        return soc_plan_max, soc_plan_min

    def calculate_relative_power_with_efficiencies(
            self, *, p_plan: List[float], power_rel_max: List[float],
            power_rel_min: List[float]) \
            -> Tuple[List[float], List[float]]:
        """
        The relative power needs to include efficiencies as well depending on
        the current absolute power
        :param p_plan: planned schedule [W]
        :param power_rel_max: the relative maximal power [W]
        :param power_rel_min: the relative minimal power [W]
        :return: soc_plan_max, soc_plan_min: the change of the SOC based on
        the maximal and minimal absolute power
        """

        period = len(p_plan)

        power_eff_max = np.zeros(period)
        power_eff_min = np.zeros(period)

        for i in range(len(p_plan)):
            if p_plan[i] >= 0:
                power_eff_max[i] = power_rel_max[i] * self.efficiency_charge
            elif self.absolute_flexibility.flex_max_power[i] > 0:
                power_eff_max[i] = \
                    self.absolute_flexibility.flex_max_power[i]* \
                    self.efficiency_charge - p_plan[i]
            else:
                power_eff_max[i] = power_rel_max[i]

            if p_plan[i] <= 0:
                power_eff_min[i] = \
                    power_rel_min[i] * 1/self.efficiency_discharge
            elif self.absolute_flexibility.flex_min_power[i] < 0:
                power_eff_min[i] = \
                    self.absolute_flexibility.flex_min_power[i] * \
                    1/self.efficiency_discharge - p_plan[i]
            else:
                power_eff_min[i] = power_rel_min[i]

        return power_eff_max, power_eff_min


    def correct_soc_min_rel(
            self, *, max_soc: List[float], min_soc: List[float],
            allowed_min_power: List[float])\
        -> List[float]:
        """
        Step 3: increases the min_soc to respect the discharge efficiency
        :param max_soc: list of max soc values [0,1]
        :param min_soc: list of min soc values [0,1]
        :param allowed_min_power: allowed min power calculated in step 4
        :return: increased_min_soc
        """

        assert len(min_soc) == len(max_soc) == len(allowed_min_power)

        period = len(allowed_min_power)

        increased_min_soc = [0.0] * len(min_soc)

        for current_interval in range(period):
            if current_interval == 0:
                increased_min_soc[current_interval] = min_soc[current_interval]
                continue
            else:
                # go back in time
                for earlier_interval in range(current_interval, -1, -1):
                    if earlier_interval == current_interval:
                        # initialize max_soc_before_current_min
                        # which contains the max_soc from which min_soc
                        # of the current_interval can be reached
                        max_soc_before_current_min = \
                            min_soc[current_interval]
                    else:
                        # increase max_soc_before_current_min
                        # due to discharge power
                        max_soc_before_current_min = \
                            max_soc_before_current_min - \
                            min(0,allowed_min_power[earlier_interval]) / \
                            self.efficiency_discharge * \
                            self.res / self.capacity_energy
                        # stop in case of mandatory charging or max_soc reached
                        if allowed_min_power[earlier_interval] > 0 or \
                                max_soc_before_current_min > \
                                max_soc[earlier_interval]:
                            max_soc_before_current_min = \
                                min(max_soc_before_current_min,
                                    max_soc[earlier_interval])
                            break
                delta_soc_till_current_interval = \
                    max_soc_before_current_min - min_soc[current_interval]
                delta_soc_till_current_interval = \
                    max(0, delta_soc_till_current_interval)
                # Increase by (1/eta-1)
                # increased_min_soc[current_interval] = \
                #     min_soc[current_interval] + \
                #     delta_soc_till_current_interval* \
                #     (1/self.efficiency_discharge-1)
                # Increase by (1-eta)
                increased_min_soc[current_interval] = \
                    min_soc[current_interval] + \
                    delta_soc_till_current_interval * \
                    (1 - self.efficiency_discharge)
                # Don't increase
                # increased_min_soc[current_interval] = min_soc[current_interval]

                increased_min_soc[current_interval] = \
                    min(increased_min_soc[current_interval],
                        max_soc[current_interval])

        return increased_min_soc

    def calculate_relative_flex(
            self, *, p_plan: List[float], visualization: bool = False) \
            -> ExtendedFlexibility:
        """
        The relative flexibility is the remaining flexibility after serving a
        planned schedule.
        Performs all flex calculation steps and stores them in a namedtuple
        :param p_plan: planned schedule [W]
        :param visualization: enables visualization of different SOCs
        :return: namedtuple Flexibility
        """

        if self.absolute_flexibility is None:
            print("Absolute flexibility need to be calculated first!")
            return None

        # Step 0: Check if planned schedule is in allowed range of
        # absolute flexibility
        if self.check_planned_schedule(p_plan = p_plan) is not True:
            print(
                "The planned schedule is not within the absolute flexibility.")
            return None

        # Step 1a: Compute relative power band
        power_rel_max, power_rel_min = \
            self.calculate_relative_power_flex(p_plan = p_plan)

        # Step 1b: Include efficiencies into the power band
        soc_plan_max, soc_plan_min = \
            self.calculate_planned_soc_change(p_plan = p_plan)
        power_eff_max, power_eff_min = \
            self.calculate_relative_power_with_efficiencies(
                p_plan=p_plan,
                power_rel_max=power_rel_max,
                power_rel_min=power_rel_min)

        # Step 2a: Calculate reachable SoC range (forward integration)
        soc_max_for, soc_min_for = \
            self.calculate_soc_flexibility_forward(
                available_max_power=power_eff_max,
                available_min_power=power_eff_min,
                planned_soc_change_max = soc_plan_max,
                planned_soc_change_min = soc_plan_min,
                relative = True
            )

        # Step 2b: Calculate required SoC range (backward integration)
        soc_max_back, soc_min_back = \
            self.calculate_soc_flexibility_backward(
                available_max_power=power_eff_max,
                available_min_power=power_eff_min,
                planned_soc_change_max=soc_plan_max,
                planned_soc_change_min=soc_plan_min,
                relative = True
            )

        # Step 2c: Combine 2a and 2b to allowed SoC range
        soc_max_rel, soc_min_rel = \
            self.calculate_allowed_soc_range(
                required_max_soc=soc_max_for,
                required_min_soc=soc_min_for,
                reachable_max_soc=soc_max_back,
                reachable_min_soc=soc_min_back
            )

        # Step 3: Calculate allowed power flexibility
        power_rel_max, power_rel_min  = \
            self.calculate_allowed_power_flexibility(
                allowed_max_soc=soc_max_rel,
                allowed_min_soc=soc_min_rel,
                available_max_power=power_rel_max,
                available_min_power=power_rel_min,
                p_plan=p_plan, relative = True
            )


        # Step 4: Convert to energy flexibility/delta energy
        soc_min_rel_corr = \
            self.correct_soc_min_rel(
                max_soc=soc_max_rel,
                min_soc=soc_min_rel,
                allowed_min_power=self.absolute_flexibility.flex_min_power)
        allowed_max_energy_delta, allowed_min_energy_delta = \
            self.convert_soc_range_to_energy_flex(
                max_soc=soc_max_rel,
                min_soc=soc_min_rel_corr
            )

        # Visualization
        if visualization:
            fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True)

            # SOC forward
            plot_limits(
                soc_max_for, soc_min_for, ax1, 'SOC forward', self.res)

            # SOC backward
            plot_limits(
                soc_max_back, soc_min_back, ax2, 'SOC backward', self.res)

            # SOC forward + backward
            plot_limits(
                soc_max_rel, soc_min_rel, ax3, 'SOC forward + backward',
                self.res)

            # SOC corrected
            plot_limits(
                soc_max_rel, soc_min_rel_corr, ax4, 'SOC corrected', self.res)

            fig.suptitle('SOC for relative flexibility', fontsize='xx-large')

        # Write values to output flexibility
        self.relative_flexibility = ExtendedFlexibility(
            flex_max_power=power_rel_max,
            flex_min_power=power_rel_min ,
            flex_max_soc=soc_max_rel,
            flex_min_soc=soc_min_rel,
            max_plan_soc=soc_plan_max,
            min_plan_soc=soc_plan_min,
            flex_max_energy_delta=allowed_max_energy_delta,
            flex_min_energy_delta=allowed_min_energy_delta
        )

        return self.relative_flexibility