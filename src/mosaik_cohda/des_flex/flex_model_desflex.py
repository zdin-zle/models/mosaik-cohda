from .modify_parameters_desflex import convert_opentum_params
from .flex_calculator import FlexCalculator
from .flex_visualization import visualize_flex

import numpy as np
import matplotlib.pyplot as plt

'''
Calculates electrical flexibility out of operation plans and devices parameters
for discretized time steps.
'''

def flex_desflex(opentum_res, flex_calc: bool):
    """
    If flex_calc is true, compute the desflex based on the opentum_res as input
    :param opentum_res: Results of opentum computation
    :param flex_calc: bool to decide if computation should happen
    """
    # Time factor for conversion calculations
    visualization = False

    if flex_calc:
        desflex_devices, t_fac = convert_opentum_params(opentum_res)
        desflex_res = compute_desflex(desflex_devices, t_fac, visualization)
    else:
        desflex_res = 'desflex flexibility calculation is turned off.'

    return desflex_res

def compute_desflex(desflex_devices, t_fac, visualization = False):
    """
    Compute desflex model for desflex devices
    :param desflex_devices: Input parameter of devices
    :param t_fac: Time factor of seconds per interval
    :param visualization: bool to decide if visualization should happen
    """
    desflex_flex = {}

    # Calculate flexibility
    for device in desflex_devices:
        # Create FlexCalculator
        flex_calculator: FlexCalculator = FlexCalculator(
            max_p_stor=desflex_devices[device]['max_p_stor'],
            min_p_stor=-desflex_devices[device]['min_p_stor'],
            capacity_energy=desflex_devices[device]['cap'],
            efficiency_charge=desflex_devices[device]['eta_el'],
            efficiency_discharge=desflex_devices[device]['eta_el'])

        # Compute absolut flex without considering the planned schedule
        abs_flex = flex_calculator.calculate_absolute_flex(
            forecast = desflex_devices[device]['forecast'],
            p_max_ps = desflex_devices[device]['p_max_ps'],
            start_soc=desflex_devices[device]['SOC_Start'],
            final_min_soc=0,
            final_max_soc=1,
            res=t_fac
        )

        # Compute relative flex by including the planned schedule
        flex_result = flex_calculator.calculate_relative_flex(
            p_plan=desflex_devices[device]['opt_plan'],
            visualization = visualization
        )

        if visualization:
            visualize_flex(abs_flex, 'Absolute flexibility',t_fac)
            visualize_flex(flex_result, 'Relative flexibility',t_fac)
            plt.show()

        desflex_flex[device] = flex_result

    # Store results
    desflex_res = {'desflex_devices': desflex_devices,
                   'desflex_flex': desflex_flex}

    return desflex_res
