import numpy as np

'''The device parameters from OpenTUMFlex are modified/transformed to fit into 
the unified model formulation of desflex.'''


def convert_opentum_params(opentum_res):
    t_fac = opentum_res['time_data']['t_inval'] * 60
    desflex_devices = {}
    for device in opentum_res['devices']:
        # extract data of chosen device
        params = opentum_res['devices'][device]
        opt_op_plan = opentum_res['optplan']

        # create modified parameter dict in model fitting format
        params_mod = {'max_p_bat': float(0), 'min_p_bat': float(0),
                      'cap': float(0), 'eta_el': float(0),
                      'opt_plan': float(0),
                      'forecast': float(0), 'p_max_ps': float(0),
                      'SOC_Start': float(0)}

        # fill modified parameter dict
        # Battery
        if device == 'bat':
            params_mod['max_p_bat'] = params['maxpow']
            params_mod['min_p_bat'] = params['maxpow']
            params_mod['cap'] = params['stocap']
            params_mod['eta_el'] = params['eta']

            params_mod['opt_plan'] = opt_op_plan['bat_input_power'] - \
                                     opt_op_plan['bat_output_power']

            # Set forecast to zeros
            params_mod['forecast'] = np.zeros(len(params_mod['opt_plan']))
            # Set max power to p_max
            params_mod['p_max_ps'] = np.ones(len(params_mod['opt_plan'])) * \
                                     params['maxpow']
            params_mod['SOC_Start'] = params['initSOC'] / 100

            desflex_devices[device] = params_mod

        # storage 
        if device == 'storage':
            params_mod['max_p_stor'] = params['maxpow']
            params_mod['min_p_stor'] = params['maxpow']
            params_mod['cap'] = params['stocap']
            params_mod['eta_el'] = params['eta']

            params_mod['opt_plan'] = opt_op_plan['stor_input_power'] - \
                                     opt_op_plan['stor_output_power']
            
            # Set forecast to zeros
            params_mod['forecast'] = np.zeros(len(params_mod['opt_plan']))
            # Set max power to p_max
            params_mod['p_max_ps'] = np.ones(len(params_mod['opt_plan'])) * \
                                     params['maxpow']
            params_mod['SOC_Start'] = params['initSOC'] / 100

            desflex_devices[device] = params_mod
        #'TODO'


        # Combined Heat and Power
        if device == 'chp':
            print('desflex does currently not support chp')

        # Photovoltaik
        if device == 'pv':
            print('desflex does currently not support pv')

        # Electric Vehicle
        if device == 'ev':
            print('desflex does currently not support ev')

        # Heat Pump
        if device == 'hp':
            print('desflex does currently not support hp')

    return desflex_devices, t_fac
