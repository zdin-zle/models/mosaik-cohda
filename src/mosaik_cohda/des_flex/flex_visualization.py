import matplotlib.pyplot as plt
import numpy as np

from .flex_class import Flexibility

'''Visualizes Flexibility within one figure'''
def visualize_flex(flexibility: Flexibility, title, t_fac):
    """
    Visualize a flexibility with according title
    """
    # Create figure with subfigures
    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, sharex=True)

    # Flex power
    plot_limits(flexibility.flex_max_power, flexibility.flex_min_power, ax1,
                'Flex power limits', t_fac)

    # SOC plan delta
    plot_timeseries(flexibility.max_plan_soc, flexibility.min_plan_soc, ax2,
                    'Planned SOC delta', t_fac)

    # SOC
    plot_limits(flexibility.flex_max_soc, flexibility.flex_min_soc, ax3,
                'Flex SOC limits', t_fac)

    # Energy Delta
    plot_limits(flexibility.flex_max_energy_delta,
                flexibility.flex_min_energy_delta, ax4,
                'Flex delta energy limits', t_fac)

    fig.suptitle(title, fontsize='xx-large')

    return fig

def plot_limits(upper_limit, lower_limit, plot,y_label,t_fac,
                upper_line = None, lower_line = None):
    """
    Plot a limited band with upper_limit and lower_limit
    """
    width = 1
    t_max = int(24 / (t_fac/3600))
    x_max = t_max - 1
    x_ticks_count = 20
    ticks_font_size = 14
    label_font_size = 10

    t_steps = np.arange(len(upper_limit)) + 0.5
    y_max = max(1.3 * max(upper_limit), -0.3 * (min(upper_limit)))
    y_min = min(1.4 * min(lower_limit), -0.4 * (max(lower_limit)))
    plot.bar(t_steps, upper_limit, width, color = 'blue')

    # Divide lower limit
    lower_limit_positive = [x if x > 0 else 0 for x in lower_limit]
    lower_limit_negative = [x if x < 0 else 0 for x in lower_limit]
    plot.bar(t_steps, lower_limit_positive, width, color = 'white')
    plot.bar(t_steps, lower_limit_negative, width, color= 'blue')

    if upper_line is not None:
        plot.step(np.append(t_steps - 0.5, t_max),
                                         np.append(upper_line, 0),
                                         linewidth=2, where='post', color='k')
    if lower_line is not None:
        plot.step(np.append(t_steps - 0.5, t_max),
                                         np.append(lower_line, 0),
                                         linewidth=2, where='post', color='k')

    plot.set_xlim([0, t_max])
    plot.set_xticks(np.linspace(0, x_max, x_ticks_count),
               fontsize=ticks_font_size)
    plot.set_xlabel('Time steps à 15 minutes', fontsize=label_font_size)
    plot.set_ylim([y_min, y_max])
    plot.set_ylabel(y_label, fontsize=label_font_size)

    plot.grid(which='both', color='0.5', linestyle='-',
             linewidth=0.5)
    return plot

def plot_timeseries(upper_line, lower_line, plot,y_label,t_fac):
    """
    Plot a timeseries
    """
    t_max = int(24 / (t_fac/3600))
    x_max = t_max - 1
    x_ticks_count = 20
    ticks_font_size = 14
    label_font_size = 10

    t_steps = np.arange(len(upper_line)) + 0.5
    y_max = max(1.3 * max(upper_line), -0.3 * (min(upper_line)))
    y_min = min(1.4 * min(lower_line), -0.4 * (max(lower_line)))
    plot.step(np.append(t_steps - 0.5, t_max),
                                         np.append(upper_line, 0),
                                         linewidth=2, where='post', color='k')
    plot.step(np.append(t_steps - 0.5, t_max),
                                         np.append(lower_line, 0),
                                         linewidth=2, where='post', color='k')

    plot.set_xlim([0, t_max])
    plot.set_xticks(np.linspace(0, x_max, x_ticks_count),
               fontsize=ticks_font_size)
    plot.set_xlabel('Time steps à 15 minutes', fontsize=label_font_size)
    plot.set_ylim([y_min, y_max])
    plot.set_ylabel(y_label, fontsize=label_font_size)

    plot.grid(which='both', color='0.5', linestyle='-',
             linewidth=0.5)

    return plot

