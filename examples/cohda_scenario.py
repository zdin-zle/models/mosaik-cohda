"""
This file contains the mosaik scenario.  To start the simulation, just run this
script from the command line.

"""
# Need to put the main directory in the PATH
if __name__ == "__main__":
    import sys
    import os

    path = os.path.realpath(os.path.join(
        os.path.dirname(__file__), os.pardir))
    sys.path.append(path)

import os
import mosaik
import logging
from datetime import datetime
from mosaik.util import connect_many_to_one

from examples.config import * 
# We have a multi-agent system, a pickle sim, loading the flex pickle; a
# dummy sim creating start values and a database:

PARAMS_CONTROL = {'control_id': 0, 'time_resolution': STEP_SIZE}


def create_world(sim_config):
    """
    Create a World instance with the given simulation configuration.

    Args:
        sim_config (dict): Configuration for the simulation.

    Returns:
        mosaik.World: Instance of the created World.
    """
    world = mosaik.World(sim_config)
    logging.info("World created")
    return world


def create_simulators(world):
    """
    Create and start simulators within the provided World instance.

    Args:
        world (mosaik.World): Instance of the World.

    Returns:
        dict: Dictionary containing references to the started simulators.
    """
    # Start the simulators:
    db_sim = world.start('DB', step_size=STEP_SIZE, duration=DURATION)

    # Read the pickles containing the Flexibility Data
    pickle_sim = world.start('PickleSim',
                             step_size=STEP_SIZE,
                             pickle_files=FLEX_FILES,
                             duration=int(DURATION / STEP_SIZE)
                             )
    # Each simulator starts with a base operational plan. The OP is also in the pickle file
    # For this example the participants are all of the other simulators
    op_plan_sim_ = []
    for idx in range(N_SIMULATORS):
        sim_params = {"pickle_file": FLEX_FILES[idx],
                      "participants": list(range(N_SIMULATORS)),
                      "duration": int(DURATION / STEP_SIZE)}
        op_plan_sim = world.start(
            'OpPlanSim', step_size=STEP_SIZE, **sim_params)
        op_plan_sim_.append(op_plan_sim)

    # Simulator for the Multi-Agent System that connects the simulators
    mas = world.start('MAS')

    simulators = {"db": db_sim,
                  "op_plan": op_plan_sim_,
                  "pickles": pickle_sim,
                  "mas": mas}
    logging.info("Simulators created")
    return simulators


def create_models(world, simulators):
    """
    Create models based on the given simulators and the World instance.

    Args:
        world (mosaik.World): Instance of the World.
        simulators (dict): Dictionary containing references to simulators.

    Returns:
        dict: Dictionary containing references to the created models.
    """

    # Read the input dict
    db_sim = simulators["db"]
    pickle_sim = simulators["pickles"]
    mas = simulators["mas"]
    op_plan_sim_ = simulators["op_plan"]

    # Empty lists for the models
    flex_model_ = []
    op_plan_ = []
    agent_model_ = []

    database_model = db_sim.Database(filename=DB_FILE)

    # Create one model for each simulator.
    for idx in range(N_SIMULATORS):
        flex_model = pickle_sim.PickleSim()
        agent_model = mas.FlexAgent(**PARAMS_CONTROL)
        op_plan = op_plan_sim_[idx].OpPlanSim()

        # Store models in lists for later connection
        flex_model_.append(flex_model)
        op_plan_.append(op_plan)
        agent_model_.append(agent_model)

    # Return Dict

    models = {"db": database_model,
              "op_plan": op_plan_,
              "flex": flex_model_,
              "agents": agent_model_}
    logging.info("Models created")
    return models


def connect_models(world, models):
    """
    Connect models within the provided World instance.

    Args:
        world (mosaik.World): Instance of the World.
        models (dict): Dictionary containing references to models.

    Returns:
        mosaik.World: Updated World instance after connecting the models.
    """
    database_model = models["db"]
    op_plan_ = models["op_plan"]
    flex_model_ = models["flex"]
    agent_model_ = models["agents"]

    for idx in range(len(flex_model_)):
        world.connect(flex_model_[idx], agent_model_[idx], ('output', 'Flexibility'),
                      async_requests=True)

        world.connect(op_plan_[idx], agent_model_[idx], ('StartValues', 'StartValues'),
                      async_requests=True)

    connect_many_to_one(world, agent_model_, database_model, 'FlexSchedules')
    connect_many_to_one(world, op_plan_, database_model, 'OutputValue')

    return world


def run_scenario(world, duration):
    """
    Run the simulation within the provided World instance for the specified duration.

    Args:
        world (mosaik.World): Instance of the World.
        duration (int): Duration for running the simulation.
    """
    world.run(duration)
    logging.info("Scenario run completed")


def main():
    """Main function that chains the different functions to set up the model
    """
    check_create_folder(DB_FOLDER)
    check_create_folder(FLEX_DIR)
    
    world = create_world(sim_config=SIM_CONFIG)
    simulators = create_simulators(world)
    models = create_models(world, simulators)
    world = connect_models(world, models)
    run_scenario(world, duration=DURATION)


def main_compiled():
    """ 
    Compilation of the previous methods in one. Smaller code, but not fragmented for testing
    Compose the mosaik scenario and run the simulation.
    """
    # Create a World instance with our SIM_CONFIG:
    world = mosaik.World(SIM_CONFIG)

    # Start the simulators:
    db_sim = world.start('DB', step_size=STEP_SIZE, duration=DURATION)

    op_plan_sim_ = []

    # Read the pickles containing the Flexibility Data
    pickle_sim = world.start('PickleSim',
                             step_size=STEP_SIZE,
                             pickle_files=FLEX_FILES,
                             duration=int(DURATION/STEP_SIZE)
                             )
    # Each simulator starts with a base operational plan. The OP is also in the pickle file
    # For this example the participants are all of the other simulators
    for idx in range(N_SIMULATORS):
        sim_params = {"pickle_file": FLEX_FILES[idx],
                      "participants": [x for x in range(N_SIMULATORS)],
                      "duration": int(DURATION/STEP_SIZE)}
        op_plan_sim = world.start(
            'OpPlanSim', step_size=STEP_SIZE, **sim_params)
        op_plan_sim_.append(op_plan_sim)

    # Simulator for the Multi-Agent System that connects the simulators
    mas = world.start('MAS')

    # Empty lists for the models
    flex_model_ = []
    op_plan_ = []
    agent_model_ = []

    database_model = db_sim.Database(filename=DB_FILE)

    for idx in range(N_SIMULATORS):
        flex_model = pickle_sim.PickleSim()
        agent_model = mas.FlexAgent(**PARAMS_CONTROL)
        flex_model_.append(flex_model)

        op_plan = op_plan_sim_[idx].OpPlanSim()
        op_plan_.append(op_plan)

        # Remember the FlexAgents entity for connecting it to the DB later:
        agent_model_.append(agent_model)

    for idx in range(N_SIMULATORS):
        world.connect(flex_model_[idx], agent_model_[idx], ('output', 'Flexibility'),
                      async_requests=True)

        world.connect(op_plan_[idx], agent_model_[idx], ('StartValues', 'StartValues'),
                      async_requests=True)

    connect_many_to_one(world, agent_model_, database_model, 'FlexSchedules')
    connect_many_to_one(world, op_plan_, database_model, 'OutputValue')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
