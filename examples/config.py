from datetime import datetime
import os

# Simulation configuration for testing
SIM_CONFIG = {
    'MAS': {
        'python': 'mosaik_cohda.cohda_simulator:CohdaSimulator'
    },
    'OpPlanSim': {
        'python': 'mosaik_cohda.pickles.op_plan_sim:OpPlanSim'
    },
    'PickleSim': {
        'python': 'mosaik_cohda.pickles.pickle_sim:PickleSim'
    },
    'DB': {
        'python': 'mosaik_hdf5:MosaikHdf5',
    },
}

# Define some constants with configuration for the simulator and the MAS. 
# Simulation Configuration :
DURATION = 12*60*60  # 12 hour
START = '2020-01-01 00:00:00'
N_SIMULATORS = 6
STEP_SIZE = 3600
TIME_STAMP = datetime.now().strftime("%Y%m%d%H%M")

# Files with flexibility example data :
EXAMPLE_FOLDER = os.path.join(os.getcwd(), "examples")
FLEX_DIR = os.path.join(EXAMPLE_FOLDER, "OP_PLAN")
FLEX_FILES = {i: os.path.join(FLEX_DIR, f"flexibility_{i}.pickle")
              for i in range(N_SIMULATORS)}

# Database config
DB_FOLDER = os.path.join(EXAMPLE_FOLDER, "DB")
DB_FILE = os.path.join(EXAMPLE_FOLDER, "DB", f"Test_Results_DB_{TIME_STAMP}.hdf5")

# Verbose if needed
VERBOSE = False




def check_create_folder(folder_path):
    """
    Checks if a folder exists at the specified path. If not, creates the folder.

    Args:
    - folder_path (str): Path of the folder to be checked/created.

    Returns:
    - None
    """
    if not os.path.exists(folder_path):
        try:
            os.makedirs(folder_path)
            print(f"Folder '{folder_path}' created successfully!")
        except OSError as e:
            print(f"Error creating folder '{folder_path}': {e}")
    else:
        print(f"Folder '{folder_path}' already exists.")
