import logging
import pickle
import time
import asyncio
import os
# Need to put the main directory in the PATH

if __name__ == "__main__":
    import sys
    path = os.path.realpath(os.path.join(
        os.path.dirname(__file__), os.pardir))
    sys.path.append(path)

from mosaik_cohda.des_flex.flex_class import Flexibility
from mosaik_cohda.start_values import StartValues
from mosaik_cohda.cohda_simulator import CohdaSimulator
from examples.config import FLEX_FILES, FLEX_DIR, DURATION, N_SIMULATORS, check_create_folder


def mango_connection():
    """
    Function to test the mango connection and the creation of the
    optimized flexibility plans
    """
    check_create_folder(FLEX_DIR)

    # Load flex
    pickle_file = FLEX_FILES[0]
    loaded_flex = pickle.load(open(pickle_file, 'rb'))
    flex = Flexibility(
        flex_max_power=loaded_flex['flex_max_power'],
        flex_min_power=loaded_flex['flex_min_power'],
        flex_max_energy_delta=loaded_flex['flex_max_energy_delta'],
        flex_min_energy_delta=loaded_flex['flex_min_energy_delta']
    )

    # Create StartValues
    schedule = [0] * int(DURATION/3600)
    for i in [2,4,7,8]:
        schedule[i] = 30000

    start_values = StartValues(schedule=schedule, participants=[i for i in range(6)])

    # Create MosaikSimulator
    cohda_sim = CohdaSimulator()    
    logging.info("Cohda SImulator initialized")
    params = {}

    # Initialize the Simulator
    cohda_sim.init(sid=0)
    params = {'control_id': 0, 'time_resolution': 60*60}
    
    #Create all the agents
    agent_model_=[]
    for i in range(N_SIMULATORS):
        agent_model = cohda_sim.create(1, 'FlexAgent', **params)
        agent_model_.append(agent_model)

    logging.info("Setup of agents done")
    cohda_sim.setup_done()

    # Provide Start Values and Input Data
    input_data = {}
    for agent in agent_model_:
        eid = agent[0]['eid']
        if True: #eid == 'Agent_0':
            data = {'StartValues': {'ID_0': start_values},
                    'Flexibility': {'ID_0': flex}
                    }
        input_data[eid] = data

    logging.info("Setting up of agents done. Running first step")
    cohda_sim.step(time=0, inputs=input_data, max_advance=0)
    time.sleep(1)

    logging.info("Simulation started succesfully. Shutting down")


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    mango_connection()
