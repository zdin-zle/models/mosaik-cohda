from setuptools import find_packages, setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

# keep this file in order to be able to run "pip install -e path/fmm" for better use of this project inside others
setup(name='mosaik-cohda',
      version='0.2.0',
      description='An simulator for mosaik for integration of flexibility'
                  'calculations with operational plans by integrating'
                  'which enables the use of mango-COHDA with mosaik',
      url='https://gitlab.com/zdin-zle/models/mosaik-cohda',
      author='Fernando Penaherrera, Jan Philip Hörding, Stephan Ferenz, Rico Schrage',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      install_requires=requirements,
      )
