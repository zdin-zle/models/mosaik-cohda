import unittest
from unittest.mock import patch
import io
import sys
import os
import mosaik

path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir,))
sys.path.append(path)
print(path)
from examples.cohda_scenario import *
from examples.config import SIM_CONFIG, DURATION
# Assuming your code is in a file named 'mosaik_scenario.py'

class TestMosaikScenario(unittest.TestCase):

    def test_run_scenario(self):
        sim_config = SIM_CONFIG
        world = create_world(sim_config)
        simulators = create_simulators(world)
        models = create_models(world, simulators)
        connected_world = connect_models(world, models)
        run_scenario(connected_world, duration=DURATION)


if __name__ == '__main__':
    unittest.main()
