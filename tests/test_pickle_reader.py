import unittest
import os
from unittest.mock import MagicMock
from mosaik_cohda.pickles.pickle_sim import PickleSim  # Replace 'your_module_name' with the actual module name

PICKLE_FILE = os.path.join(os.getcwd(), "examples", "OP_PLAN", "flexibility_0.pickle")

class TestPickleSim(unittest.TestCase):
    def setUp(self):
        # This is called in every function
        self.pickle_sim = PickleSim()

    def test_init(self):
        # Test init method with valid step_size
        self.pickle_sim.init(sid='test_sid',
                             step_size=3600,
                             pickle_files={0: PICKLE_FILE})
        self.assertEqual(self.pickle_sim.step_size, 3600)

        # Test init method with invalid step_size
        with self.assertRaises(ValueError):
            self.pickle_sim.init(sid='test_sid',
                                 step_size=1800,
                                 pickle_files={0: PICKLE_FILE})

    def test_create(self):
        # Test create method
        self.pickle_sim.init(sid='test_sid',
                             step_size=3600,
                             pickle_files={0: PICKLE_FILE, 1: PICKLE_FILE})
        entities = self.pickle_sim.create(num=2, model='flex')
        expected_entities = [
            {'eid': 'pickels-0', 'type': 'flex'},
            {'eid': 'pickels-1', 'type': 'flex'}]
        self.assertEqual(entities, expected_entities)

    def test_step(self):
        # Test step method
        # Mock the return value of the step method for demonstration purposes
        self.pickle_sim.step = MagicMock(return_value=123456)
        time = 1000
        inputs = {}
        max_advance = 3600
        new_time = self.pickle_sim.step(time, inputs, max_advance)
        self.assertEqual(new_time, 123456)

    def test_get_data(self):
        # Test get_data method
        # Mock cache data for demonstration purposes
        self.pickle_sim.init(sid='test_sid',
                             step_size=3600,
                             pickle_files={0: PICKLE_FILE, 1: PICKLE_FILE})
        self.pickle_sim.create(num=2, model='flex')        
        self.pickle_sim.cache = {'pickels-0': 'data_0', 'pickels-1': 'data_1'}
        outputs = {'pickels-0': ['output']}
        data = self.pickle_sim.get_data(outputs)
        expected_data = {'pickels-0': {'output': 'data_0'}}
        self.assertEqual(data, expected_data)


if __name__ == '__main__':
    unittest.main()
