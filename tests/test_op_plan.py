import os
import unittest
from mosaik_cohda.pickles.op_plan_sim import OpPlanSim 


class TestOpPlanSim(unittest.TestCase):

    def setUp(self):
        self.op_plan_sim = OpPlanSim()

    def test_init(self):
        self.assertEqual(self.op_plan_sim.step_size, 3600)
        self.assertEqual(self.op_plan_sim.cache_dict, {})
        self.assertEqual(self.op_plan_sim.step_counter, 0)

    def test_init_with_meta(self):
        self.assertIsNotNone(self.op_plan_sim.meta)

    def test_init_with_sid(self):
        sid = 'test_sid'
        print(os.getcwd())
        sim_params = {"pickle_file": os.path.join(os.getcwd(),
                                                  "examples",
                                                  "OP_PLAN",
                                                  "flexibility_0.pickle"),
                       "participants": [0]}
        meta = self.op_plan_sim.init(sid, **sim_params)
        self.assertEqual(meta, self.op_plan_sim.meta)
        self.assertEqual(self.op_plan_sim.step_size, 3600)

    def test_init_with_invalid_step_size(self):
        with self.assertRaises(ValueError):
            self.op_plan_sim.init('test_sid', step_size=7200)

    def test_init_without_pickle_file(self):
        with self.assertRaises(ValueError):
            self.op_plan_sim.init('test_sid', step_size=3600, participants=[])

    def test_create(self):
        num = 1
        model = 'TestModel'
        entities = self.op_plan_sim.create(num, model)
        self.assertEqual(len(entities), num)
        self.assertIn('eid', entities[0])
        self.assertIn('type', entities[0])
        self.assertEqual(entities[0]['type'], model)

    def test_create_multiple_instances(self):
        with self.assertRaises(ValueError):
            self.op_plan_sim.create(2, 'TestModel')

    def test_get_data(self):
        self.op_plan_sim.op_plan = {'op_plan-0': 0}
        self.op_plan_sim.cache_dict = {'schedule': [1, 2, 3]}
        outputs = {'op_plan-0': ['OutputValue']}
        data = self.op_plan_sim.get_data(outputs)
        self.assertIn('op_plan-0', data)
        self.assertIn('OutputValue', data['op_plan-0'])
        self.assertEqual(data['op_plan-0']['OutputValue'], 3)

    def test_get_data_with_unknown_entity_id(self):
        with self.assertRaises(ValueError):
            self.op_plan_sim.get_data({'unknown_entity': ['OutputValue']})

    def test_get_data_with_invalid_attribute(self):
        self.op_plan_sim.op_plan = {'op_plan-0': 0}
        with self.assertRaises(AttributeError):
            self.op_plan_sim.get_data({'op_plan-0': ['InvalidAttribute']})

    def test_step(self):
        time = 0
        inputs = {}
        max_advance = 3600
        new_time = self.op_plan_sim.step(time, inputs, max_advance)
        self.assertEqual(new_time, time + 3600)
        self.assertEqual(self.op_plan_sim.step_counter, 1)


if __name__ == '__main__':
    unittest.main()
