import unittest
from unittest.mock import patch
import io
import sys
import os
import mosaik

path = os.path.realpath(os.path.join(
    os.path.dirname(__file__), os.pardir))
sys.path.append(path)


from examples.mango_connection import mango_connection

class TestMosaikScenario(unittest.TestCase):

    def test_mango_connectionrun_scenario(self):
        mango_connection()

if __name__ == '__main__':
    unittest.main()
