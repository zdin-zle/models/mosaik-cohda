.. Mosaik-Cohda documentation master file, created by
   sphinx-quickstart on Fri Dec  1 11:11:04 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Mosaik-Cohda's documentation!
========================================
**Mosaik-COHDA** is a Python library that implements a simulator for the 
energy storage optimization for COHDA.

It implements the algorithms for generating multi criteria optimized schedules for
integrating storages with wide sets of objectives in a COHDA negotiation.

Check out the :doc:`usage` section for further information, including how to 
:ref:`install <repository_installation>` the repository and
how to install the repository as a
:ref:`dependency <pckg_installation>`.

Additionally, you can run an example simulation of `mosaik-cohda` following the
:ref:`simulation <mosaik_example>` example
and also test the connection to the Multi Agent System - developed with mango - following the
:ref:`connection <cohda_example>` example.

.. note:: 

   This project is under active development from the `ZLE Research group <https://www.zdin.de/zukunftslabore/energie>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   autoapi/index
   usage
   examples
   structure
   authors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

