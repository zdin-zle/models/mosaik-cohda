Authors & Acknowledgments
========================================

Authors
----------------------------------------
`Fernando Peñaherrera V. <fernando.andres.penaherrera.vaca@offis.de>`_, OFFIS Institute of Information Technology

`Jan Philipp Hörding <jan.philipp.hoerding@offis.de>`_, OFFIS Institute of Information Technology

Funding
----------------------------------------
This research was funded by the Lower Saxony Ministry of Science and Culture under grant number 11-76251-13-3/19 ZN3488 (ZLE) within the Lower Saxony “SPRUNG“ of the Volkswagen Foundation. It was supported by the Center for Digital Innovations (ZDIN).

Licence
----------------------------------------
`MIT License <https://gitlab.com/zdin-zle/models/mosaik-cohda/-/blob/main/LICENSE?ref_type=heads>`_
