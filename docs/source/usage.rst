Usage
========================================

.. _repository_installation:

Repository Installation
----------------------------------------

To use the models provided in this repository, follow these steps:

1. Clone the repository:

   .. code-block:: bash

      git clone https://gitlab.com/zdin-zle/models/mosaik-cohda.git

2. Install the necessary dependencies. Refer to the `requirements.txt` file for a list of required packages and their versions:

   .. code-block:: bash

      pip install -r requirements.txt


3. To run the examples from the  `examples` folder, the source code can be installed in the environment using:

   .. code-block:: bash

      pip install -e .


.. _pckg_installation:
Installation as package
----------------------------------------

1. Install using pip from the gitlab repository:

   .. code-block:: bash
   
      pip install git+https://gitlab.com/zdin-zle/models/mosaik-cohda.git


2. It can be also includded as a dependency on the `requirements.txt` file. Simply add this line to your requirements.txt file, and users can install this repository directly using pip by specifying the requirements file:

   .. code-block:: bash
   
      git+https://gitlab.com/zdin-zle/models/mosaik-cohda.git



   .. code-block:: bash

      pip install -r requirements.txt



