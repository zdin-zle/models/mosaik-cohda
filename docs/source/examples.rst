Examples
========================================

.. _mosaik_example:

Example of Simulation using Mosaik and Cohda
----------------------------------------

These steps allow running an example using mosaik_cohda:

1. Clone the repository:

   .. code-block:: bash

      git clone https://gitlab.com/zdin-zle/models/mosaik-cohda.git

2. Install the necessary dependencies. Refer to the `requirements.txt` file for a list of required packages and their versions:

   .. code-block:: bash

      pip install -r requirements.txt


3. To run the examples from the  `examples` folder, the source code can be installed in the environment using:

   .. code-block:: bash

      pip install -e .

4. Go to the examples folder, where the file `cohda_scenario.py` is located:

   .. code-block:: bash

      cd examples

5. Run the file `cohda_scenario.py`:

   .. code-block:: bash

      python cohda_scenario.py


.. _cohda_example:

Testing the Mosaik-Mango Connection
----------------------------------------

To test the connection of mosaik with the Multi Agent System created with mango, repeat the previous installation steps (1-4), and
run the file `mango_connection.py`:

   .. code-block:: bash

      python mango_connection.py

