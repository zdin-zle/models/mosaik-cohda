Structure
========================================

Simulation Architecture
----------------------------------------
This software is developed to be coupled with `mosaik <https://mosaik.offis.de/>`_. It is based on the implementation of the mosaik API.


.. code-block:: python

    import mosaik_api

    class CohdaSimulator(mosaik_api.Simulator):
        """
        Interface of the Cohda Simulator to the Mosaik API.
        """

        def __init__(self):

    ...

This interface creates a list of agents and a container that links the mosaik API to the
`mango <https://github.com/OFFIS-DAI/mango>`_ API. The class `MosaikAgent` serves
as a gateway between the mosaik API and the `Flexibility Agents`. It forwards the current state of the simulated devices to the agents,
triggers the controller, waits for the controller to be done, and then collects new set-points for the simulated devices from the agents.


Installed Dependencies
----------------------------------------
Several packages were cloned and its dependencies adapted to match the structure of this repository.

Mango Library
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The `mango-library <https://gitlab.com/mango-agents/mango-library/-/tree/b235935fb02931161f52ce2aec9a3dedbcd707c9>`_ is part of
mango (modular python agent framework). mango is a Python library for multi-agent systems (MAS).
It is written on top of asyncio and is released under the MIT license.
mango allows the user to create simple agents with little effort and at the same time offers options
to structure agents with complex behavior.

A detailed documentation for this project can be found at the mango `documentation <mango-agents.readthedocs.io>`_.

DES Flexibility Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The DES flexibility model `des_flex <https://gitlab.com/digitalized-energy-systems/models/reamplify/-/tree/4cea9fe7c857905125caf5f79c806c262eb0b34d>`_
is part of the ReAmplify (Relative Abstract Multi-Purpose-Limited Flexibility) model. 
It is compatible with the comparison of flexibility models by Brandt et al. which can relate to the implementation of an
`unified flexibility scenario <https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario>`_.


EO-COHDA
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`eocohda <https://gitlab.com/digitalized-energy-systems/models/eo-cohda/-/tree/633f40f58e2c6dc83bd07871579606de082ffc2a>`_
stands for "Energy Storage Optimization for COHDA" and contains several algorithms for
generating multi-criteria optimized schedules for integrating storages with wide sets of objectives
in a COHDA negotiation. Furthermore, the repository provides an implementation of the integration into COHDA itself,
designed as a multi-process optimization with aiomas as an agent framework and ISAAC as a COHDA variant.

Flex-COHDA
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`flex_cohda <https://gitlab.com/digitalized-energy-systems/models/flex-cohda/-/tree/1ae16691bc9c1f8c8dd42021f50228d050b1e13a>`_
contains several algorithms for generating multi-criteria optimized schedules for integrating
flexibility with a wide sets of objectives in a COHDA negotiation.
