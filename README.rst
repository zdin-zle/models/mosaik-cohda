Welcome to Mosaik-Cohda's documentation!
========================================
**Mosaik-COHDA** is a Python library that implements a simulator for the 
energy storage optimization for COHDA

It implements the algorithms for generating multi criteria optimized schedules for
integrating storages with wide sets of objectives in a COHDA negotiation

Check out the usage_ section for further information, including how to 
install_ the repository and 
how to install the repository as 
package_

Additionally, you can run an example simulation of `mosaik_cohda` following the 
`simulation <https://zdin-zle.gitlab.io/models/mosaik-cohda/examples.html#example-of-simulation-using-mosaik-and-cohda>`_ example
and also test the connection to the Multi Agent System - mango - following the
`connection <https://zdin-zle.gitlab.io/models/mosaik-cohda/examples.html#testing-the-mosaik-mango-connection>`_ example

The complete documentation can be visited under in the `Documentaion <https://zdin-zle.gitlab.io/models/mosaik-cohda/index.html>`_ website.

Usage
========================================

.. _install:
Repository Installation
----------------------------------------

To use the models provided in this repository, follow these steps:

1. Clone the repository:

   .. code-block:: bash

      git clone https://gitlab.com/zdin-zle/models/mosaik-cohda.git

2. Install the necessary dependencies. Refer to the `requirements.txt` file for a list of required packages and their versions:

   .. code-block:: bash

      pip install -r requirements.txt


3. To run the examples from the  `examples` folder, the source code can be installed in the environment using:

   .. code-block:: bash

      pip install -e .


.. _package:
Installation as package
----------------------------------------

1. Install using pip from the gitlab repository:

   .. code-block:: bash
   
      pip install git+https://gitlab.com/zdin-zle/models/mosaik-cohda.git


2. It can be also includded as a dependency on the `requirements.txt` file. Simply add this line to your requirements.txt file, and users can install this repository directly using pip by specifying the requirements file:

   .. code-block:: bash
   
      git+https://gitlab.com/zdin-zle/models/mosaik-cohda.git



   .. code-block:: bash

      pip install -r requirements.txt


Structure
========================================

Simulation Architecture
----------------------------------------
This software is developed to be coupled with `mosaik <https://mosaik.offis.de/>`_. It is based on the implementation of the mosaik API.


.. code-block:: python

    import mosaik_api

    class CohdaSimulator(mosaik_api.Simulator):
        """
        Interface of the Cohda Simulator to the Mosaik API.
        """

        def __init__(self):

    ...

This interface creates a list of agents and a container that link the mosaik API to the
`mango <https://github.com/OFFIS-DAI/mango>`_ API. The class `MosaikAgent` serves
as a gateway between the mosaik API and the Flexibility Agents. It forwards the current state of the simulated devices to the agents,
triggers the controller, waits for the controller to be done and then collects new set-points for the simulated devices from the agents.


Installed Dependencies
----------------------------------------
Several packages were cloned and its depenencies adapted to match the structure of this repository.

Mango Library
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`mango-library <https://gitlab.com/mango-agents/mango-library/-/tree/b235935fb02931161f52ce2aec9a3dedbcd707c9>`_ is part of 
mango (modular python agent framework). mango is a python library for multi-agent systems (MAS).
It is written on top of asyncio and is released under the MIT license.
mango allows the user to create simple agents with little effort and in the same time offers options
to structure agents with complex behaviour.

A detailed documentation for this project can be found at the mango `documentation <mango-agents.readthedocs.io>`_

DES Flexibility Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The DES flexibility model `des_flex <https://gitlab.com/digitalized-energy-systems/models/reamplify/-/tree/4cea9fe7c857905125caf5f79c806c262eb0b34d>`_
is part of the ReAmplify (Relative Abstract Multi-Purpose-Limited Flexibility) model. 
It is compatible with the comparison of flexibility modeles by Brandt et al. which can relate to the implementation of an 
`unified flexibility scenario <https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario>`_


EO-COHDA
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`eocohda <https://gitlab.com/digitalized-energy-systems/models/eo-cohda/-/tree/633f40f58e2c6dc83bd07871579606de082ffc2a>`_
stands for "Energy Storage Optimization for COHDA" and contains several algorithms for 
generating multi criteria optimized schedules for integrating storages with wide sets of objectives 
in a COHDA negotiation. Furthermore the repository provides an implementation of the integration into COHDA itself, 
designed as multi-process optimization with aiomas as agent framework and ISAAC as COHDA variant.

Flex-COHDA
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`flex_cohda <https://gitlab.com/digitalized-energy-systems/models/flex-cohda/-/tree/1ae16691bc9c1f8c8dd42021f50228d050b1e13a>`_
contains several algorithms for generating multi criteria optimized schedules for integrating
flexibility with wide sets of objectivesin a COHDA negotiation.


Authors & Acknowledgments
========================================

Authors
----------------------------------------
`Fernando Peñaherrera V. <fernando.andres.penaherrera.vaca@offis.de>`_, OFFIS Institute of Information Technology

`Jan Philipp Hörding <jan.philipp.hoerding(@offis.de>`_, OFFIS Institute of Information Technology

Funding
----------------------------------------
This research was funded by the Lower Saxony Ministry of Science and Culture under grant number 11-76251-13-3/19 ZN3488 (ZLE) within the Lower Saxony “SPRUNG“ of the Volkswagen Foundation. It was supported by the Center for Digital Innovations (ZDIN).

Licence
----------------------------------------
`MIT License <https://gitlab.com/zdin-zle/models/mosaik-cohda/-/blob/main/LICENSE?ref_type=heads>`_


.. note:: 

   This project is under active development from the `ZLE Research group <https://www.zdin.de/zukunftslabore/energie>`_.

